"""
This example script shows how to use pyNyx to run a simple experiment.
This script doesn't support power-off recovery. For that, use the Experiment class
"""

import pynyx
import datetime
# Set sample information
# Set manually
# sample = {'name' : 'Default',
#           'Dewar':'Red SED'}
# Or lookup from DB tools
sample = pynyx.data.get_sample_db(name='Default')
group_id = pynyx.data.get_group_id()
# Verify equipment connections
config = pynyx.data.read_config(path=None)  # From config string. Optionally pass the path to the config string
smu = pynyx.equipment.keithley(com=config['Keithley port'], defaults=config['Keithley Defaults'])  # Create object for this tool
blackbody = pynyx.equipment.SRI(com=config['SRI port'], defaults=config['SRI defaults']) # Create object for this tool
equipment = [smu, blackbody]
for eq in equipment:
    eq.check_ready()  # This method is in the base class, inherited for all others

# Set equipment configurations

for eq in equipment:
    eq.setup()  # This method is in the base class, inhereted for all others.
    # Optional KWARGS can set different things than default configs.
smu.config_sweep_iv(start=-1, step=0.1, nPLC=1)  # Sets some configurations different than the default configurations
blackbody.config_tol(tolerance=0.1, timeout=30)  # Tolerance for a stable temp
# Enter main measurement loop
for bb_temp in [500, 600, 700]:
    print(f"Now measuring sample {sample['name']} at {bb_temp}K")
    actual_temp = blackbody.set_temp(bb_temp)  # This method sets the temperature and waits for stability via tolerance
    iv_data = smu.sweep_iv()  #
    meta = {'set_T' : bb_temp,
            'read_T' : actual_temp,
            'date-time': datetime.datetime.now(),
            'group_id' : group_id}
    pynyx.data.save_iv_db(sample=sample, iv_data=iv_data, metadata=meta, db=None)  # Data module handles saving files and maintaining backups
    pynyx.data.save_iv_files(sample=sample, data=iv_data, metadata=meta, path=None)
