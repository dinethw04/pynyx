"""
This class handles all input and output from text files or databases
"""
import json
import sqlite3
import datetime
import pyodbc
import zlib, zipfile
from pynyx import setup_logger
import os
logger = setup_logger(__name__)

connection_str = """
Driver={SQL Server};
Server=SIVALABSER-001\\SQLEXPRESS;
Database=CQD;
Trusted_Connection=yes;
"""


def clean_sql(sql_str):
    # TODO smarter sql injection testing
    bad_strs = ['exec', ';', 'drop']
    flat_sql = sql_str.casefold()
    for bad_str in bad_strs:
        if bad_str in flat_sql:
            raise SyntaxError(f"SQL Injection detected, string contains {bad_str}")
    return sql_str


def query(query_str, connection=None):
    """
    Runs an SQL query over the database, with the intent to return a list of rows
    :param query_str:
    :param connection:
    :return:
    """
    query_str = clean_sql(query_str)
    if connection is None:
        conn = pyodbc.connect(connection_str)
    else:
        conn = connection
    cur = conn.cursor()
    data = cur.execute(query_str).fetchall()
    if connection is None:
        conn.close()
    return data


def insert(insert_str, connection=None):
    """
    Runs an insert string and commits
    :param insert_str:
    :param connection:
    :return:
    """
    insert_str = clean_sql(insert_str)
    if connection is None:
        conn = pyodbc.connect(connection_str)
    else:
        conn = connection
    cur = conn.cursor()
    cur.execute(insert_str).commit()
    if connection is None:
        conn.close()
    return True


def get_group_id(source=None):
    """
    Gets the latest group ID.
    :param source: If None, checks for a database first, and then checks for files. Otherwise does what you ask.
    :return:
    """
    #todo fix this
    group_id = 'temp_is_none'
    if source is None:
        pass
    elif type(source) is str:
        # source is a path to the group ids then
        pass
    elif type(source) is None: #database:
        # database type
        pass
    else:
        assert NotImplementedError
    return group_id


def get_sample_db(connection=None, simple=False, **kwargs):
    # if connection is None:
    #     conn = pyodbc.connect(connection_str)
    # else:
    #     conn = connection
    if simple:
        select = 'SELECT TOP (1) QDIP_NAME, Stock_Solution_Type FROM CQD_Deposition'
    else:
        select = 'SELECT TOP (1) * FROM CQD_Deposition'
    where = ' WHERE '
    first = True
    for key, value in kwargs.items():
        if type(value) is str:
            value = f"'{value}'"
        elif type(value) in [int, float]:
            value = f'{value:.6f}'
        else:
            raise TypeError(f"Tried to query with value {value} with datatype {type(value)} unsupported")
        if first:
            where += f"{key} = {value}\n"
            first=False
        else:
            where += f" and {key} = {value}\n"
    data = query(select + where, connection)
    logger.debug(data)

    return data[0]


def read_config(path=None):
    """
    Reads a config string and returns a config dictionary.
    :param path: Path to the config file. Probably a JSON or XML or something cool
    :return:
    """
    config = {}


def save_iv_db(sample, iv_data, metadata=None, db=None):
    """
    Saves an IV sweep to the database
    :param sample:
    :param iv_data:
    :param metadata:
    :param db:
    :return:
    """
    raise NotImplementedError


def save_iv_files(sample, iv_data, metadata=None, folder=None, delimiter='\t', single_file=False, datafile_prefix='IV '):
    return save_data_to_file(sample, iv_data, metadata, folder, delimiter, single_file, datafile_prefix)


def save_data_to_file(sample, data_frame, metadata=None, folder=None, delimiter='\t', single_file=False, datafile_prefix=""):
    """
    Saves an IV sweep to a set of text files for parsing
    :param sample: dictionary of the sample info: Name, Dewar, device structure, ink, solution
    :param data_frame: dataframe of data as returned from a measurement
    :param metadata: Related metadata, time, BB temp, dewar temp, setpoint, distance, aperture, filter, etc.
    :param path: location to save the file, or zip file. None saves to where this was executed from
    :return:
    """
    # TODO check for dictionary keys related to database tables.
    # TODO raise warning when certain values are missing
    # TODO metadata may be mixed types of floats, strings, and whatever. Check?
    if metadata is None:
        metadata = {'datetime': datetime.datetime.now().strftime('%Y-%m-%d %H%M%S')}
    full_header = {**sample, **metadata}
    summary_str = delimiter.join(full_header.keys()) + '\n' + delimiter.join(full_header.values())
    if 'datetime' in metadata:
        data_date = datetime.datetime.strptime(metadata['datetime'], '%Y-%m-%d %H%M%S')
    else:
        data_date = datetime.datetime.now()
    if 'Unique_ID' not in data_frame.columns:
        data_frame.insert(0, 'Unique_ID', create_unique_id(data_date))
    data_str = data_frame.to_csv(sep=delimiter, float_format='%.6E', index=False)
    datafile_name = datafile_prefix + f"{sample['name']}_{data_date.strftime('%Y-%m-%d %H%M%S')}"
    save_filestrings([summary_str, data_str],
                     ['Summary ' + datafile_name, 'Raw ' + datafile_name],
                     container_name=datafile_name, folder=folder, single_file=single_file)


def save_filestrings(file_str_list, file_names=None, container_name=None, folder=None, single_file=False):
    """
    Saves a list of filestrings and their names to a datafile either as a single file, zip, or folder
    :param file_str_list:
    :param file_names:
    :param container_name:
    :param folder:
    :param single_file:
    :return:
    """
    if file_names is None:
        file_names = [f"{idx:06d}" for idx in range(len(file_str_list))]
    else:
        assert len(file_names) == len(set(file_names)), "File names must all be unique"
    if container_name is None:
        container_name = file_names[0] + "_set"
    if folder is None:
        folder = os.path.join(os.getcwd())
    elif not os.path.isdir(folder):
        raise OSError
    if single_file in ['zip', '.zip', 'compress']:
        with zipfile.ZipFile(os.path.join(folder, container_name+ '.zip'), mode='w') as zf:
            for name, file_str in zip(file_names, file_str_list):
                zf.writestr(name + '.tsv', file_str)
    elif single_file is True:
        with open(os.path.join(folder, container_name + '.tsv'), 'w') as data_file:
            for file_str in file_str_list:
                data_file.write(file_str)
                data_file.write('\n\n')
    elif single_file is False:
        for name, file_str in zip(file_names, file_str_list):
            with open(os.path.join(folder, name + '.tsv'), 'w') as data_file:
                data_file.write(file_str)


def save_iv(sample, iv_data, method='both', metadata=None, path=None, db=None):
    """
    Saves an IV sweep.
    :param sample:
    :param iv_data:
    :param method:
    :param metadata:
    :param path:
    :param db:
    :return:
    """
    if method in ['both', 'file']:
        save_iv_files(sample, iv_data, metadata, path)
    if method in ['both', 'database', 'db']:
        save_iv_db(sample, iv_data, metadata, db)


def save_i_vs_t_files(sample, time_data, metadata=None, folder=None, delimiter='\t', single_file=False):
    return save_data_to_file(sample, time_data, metadata, folder, delimiter, single_file, datafile_prefix='I(t) ')


def save_ivt_files(sample, ivt_data, metadata=None, folder=None, delimiter='\t', single_file=False):
    """

    :param sample:
    :param ivt_data: This is a DataFrame which has an entry called 'raw' which is also a DataFrame
    :param metadata:
    :param folder:
    :param delimiter:
    :param single_file:
    :return:
    """
    if metadata is None:
        metadata = {'datetime': str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))}
        data_date_final = data_date.replace(":","")
        metadata['date-time'] = data_date_final
    if 'date-time' in metadata:
        data_date = str((datetime.datetime.strptime(metadata['date-time'], '%Y-%m-%d %H:%M:%S.%f')))
        data_date_final = data_date.replace(":","")
        metadata['date-time'] = data_date_final
    else:
        data_date = datetime.datetime.now()
        data_date = (data_date.strftime('%Y-%m-%d %H:%M:%S.%f'))
        data_date_final = data_date.replace(":","")
        metadata['date-time'] = data_date_final
    if 'Sweep_ID' not in metadata:
        metadata['Sweep_ID'] = "S" + create_unique_id(data_date)


    full_header = {**sample, **metadata}

    #kept getting non on index 3 so i just took it out
    #seemed like everything was still their though
    #todo check this
    sweep_summary_str = delimiter.join(full_header.keys()) + '\n' + delimiter.join(full_header.values())
    # Ignore the last column which should be the raw I(t) data.
    iv_sumamry_str = ivt_data.to_csv(sep=delimiter, float_format='%.6E', index=False, columns=ivt_data.columns[0:-1])
#    i_vs_t_str = '\n'.join([df.to_csv(sep=delimiter, float_format='%.6E', index=False) for df in ivt_data])
#did this because their is only data_frame passed to ivt_data
#so when you are iterating through ivt_data i think that you are just grabbing the data within it and not
#a actually data_frame (i got an error that made me think this. Im just leaving this comment so you can kinda
    #undertand why)
    i_vs_t_str = '\n'.join([ivt_data.to_csv(sep=delimiter, float_format='%.6E', index=False)])
    # TODO doesn't have unique ids
    datafile_name = 'IV(T)_' + f"{sample['name']}_{data_date_final}"
    return save_filestrings([sweep_summary_str, iv_sumamry_str, i_vs_t_str],
                            [pref + datafile_name for pref in ['Sweep Summary_', 'IV summary_', 'Raw_']],
                            container_name=datafile_name, folder=folder, single_file=single_file)


    # data_frame.insert(0, 'Unique_ID', create_unique_id(data_date))


def create_unique_id(now=datetime.datetime.now()):
    return now.strftime('ID%Y%m%d%H%M%S')


def create_insert_string(table, columns, values, datatypes=None):
    insert_str = 'INSERT INTO '
    raise NotImplementedError
    return insert_str