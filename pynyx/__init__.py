__all__ = ['equipment','measure', 'data']

import logging

log_dict = {'pynyx.data' : logging.DEBUG,
            'pynyx.equipment' : logging.DEBUG,
            'pynyx.measure' : logging.DEBUG
            }


def setup_logger(name, loglevel=None):
    logger = logging.getLogger(name)
    if loglevel is None:
        if name in log_dict:
            loglevel = log_dict[name]
        else:
            loglevel = logging.DEBUG
    logger.setLevel(loglevel)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(loglevel)
    formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)
    return logger
