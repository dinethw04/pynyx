import numpy as np
import pandas as pd
from pynyx import data
num_pts = 10
times = np.linspace(0, 1000, num_pts)
dfs = []
id_int = int(data.create_unique_id()[2:])
all_data = {'Sweep_ID': f"SID{id_int:d}"}
for volt in np.arange(-5, 5, 0.5):
    # raw_data = np.arange(0, 10, 0.5)
    raw_data = np.random.random(num_pts)
    stack_data = np.column_stack((times, raw_data))
    i_vs_t = pd.DataFrame(stack_data, columns=['Time_(ms)', 'Current_(A)'])
    dfs.append(i_vs_t)
    iv_dict = {'Unique_ID': f"ID{id_int:d}",
               'Sweep_ID': all_data['Sweep_ID'],
               'i_vs_t': i_vs_t,
               'Avg_Current_(A)': i_vs_t['Current_(A)'].mean(),
               'rms_Current_(A)': np.sqrt((i_vs_t['Current_(A)'] ** 2).mean()),
               'rms_Noise_Current_(A)': i_vs_t['Current_(A)'].std()}
    # iv_dataframe = pd.concat(iv_dict)
    # dfs.append(iv_dataframe)
print(len(dfs), len(range(num_pts)))
thing = pd.DataFrame({'Unique_ID': [f"ID{id_int + count:d}" for count in range(len(dfs))],
     'Voltage_(V)': list(np.arange(-5, 5, 0.5)),
     'Avg_Current_(A)': [df['Current_(A)'].mean() for df in dfs],
'2Avg_Current_(A)2': [df['Current_(A)'].mean() for df in dfs],
     'raw': [df for df in dfs]
     })
print(thing)
print(thing['Voltage_(V)'], thing['Avg_Current_(A)'])
print(thing['raw'][0])
data.save_ivt_files({'name':'dabest'}, thing, single_file=True)
# print("First entry is:", thing[0])