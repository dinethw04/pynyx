import numpy as np
import pandas as pd
raw_data = np.arange(0, 10, 0.5)
volts, currs = raw_data[::2], raw_data[1::2]  # Data is in format of Volt, Current, Volt, Current etc.
print(volts, currs)
times = [2 for _ in volts]
data = np.column_stack((volts, currs, times))
df = pd.DataFrame(data, columns=['Voltage_(V)', 'Current_(A)', 'time'])
# data[0] = volts
# data[1] = currs
print("Structured data is then")
print(df)
print("volts from dt")
print(df['Current_(A)'])
print(df.to_csv(sep='\t', float_format='%.6E', index=False))