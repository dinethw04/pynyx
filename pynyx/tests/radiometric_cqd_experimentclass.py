import pyNyx
import datetime
# Set sample information
# Set manually
# sample = {'name' : 'Default',
#           'Dewar':'Red SED'}
# Or lookup from DB tools
sample = pyNyx.io.get_sample_db(name='Default')

# Verify equipment connections is done inside the measurement class

# Setup config stuff if different than default
iv_config = {'start': -1, 'step':0.1, 'nPLC':1}
# Run radiometric tests via a measurement object
radio_test = pyNyx.measure.Radiometric(sample=sample, smu=None, iv_config=iv_config,
                                           blackbody=None, bb_temp_list=[500,600,700],
                                           config_path=None, recovery_path=None)
radio_test.run()
# This object runs the measurements, stores data as it goes, stores the state of the equipment in recovery_path, and
# pickles itself there letting you recovery if there's a loss of connection anywhere.
# Updates are printed out and set to the logger.
# other objects run other preconfigured tests. Or you can make your own via a script without using this class.