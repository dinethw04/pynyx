import serial
import time

eol = '\r'
serial_config = {  # 'port' : com_port,
    'timeout': 3, 'baudrate': 9600,
    'parity': serial.PARITY_NONE,
    'bytesize': serial.EIGHTBITS, }

def readline(serial_obj, eol='\r'):
    answer = bytearray()
    while True:
        c = serial_obj.read(1)
        if c not in eol.encode():
            answer += c
        else:
            break
    return answer


def readall2(serial_obj, delay=0):
    answer = bytearray()
    answer += serial_obj.read(1)
    time.sleep(.1)
    while serial_obj.in_waiting:
        answer += serial_obj.read(serial_obj.in_waiting)
        time.sleep(delay)
    return answer


def readall(serial_obj):
    answer = bytearray()
    while True:
        if serial_obj.in_waiting:
            c = serial_obj.read(serial_obj.in_waiting)
        else:
            c = serial_obj.read(1)
        if c:
            answer += c
        else:
            break
    return answer


def ask(msg,serial_config,com='COM4', eol='\r', serial_obj=None):
    if serial_obj is None:
        con = serial.Serial(com, **serial_config)
        con.open()
    else:
        con = serial_obj
    print(f"On {com} now asking :\n{msg}")
    con.write((msg + eol).encode())
    answer = readline(con)
    if serial_obj is None:
        con.close()
    return ''.join([chr(i) for i in answer.rstrip()])


def poll(msg,serial_config,com='com4', eol='\r', delay=0):
    with serial.Serial(com, **serial_config) as con:
        print(f"On {com} now polling :\n{msg}")
        con.write((msg + eol).encode())
        answer = readall2(con, delay=delay)
    return ''.join([chr(i) for i in answer.rstrip()])


def safe_ask(msg, serial_obj=None, delay=0.01, timeout=20):
    if serial_obj is None:
        con = serial.Serial('com4', **serial_config)
        con.open()
    else:
        con = serial_obj
    print(f"Safely asking: {msg}")
    con.write((msg+eol).encode())
    time.sleep(0.25)
    last_bytes = [-1, -1, -1]
    # Wait for message to arrive
    for i in range(int(timeout/delay)):
        if con.in_waiting and all(last == con.in_waiting for last in last_bytes):
            break
        else:
            last_bytes[i % len(last_bytes)] = con.in_waiting
            print(f"We have {con.in_waiting} bytes...")
            time.sleep(delay)
    answer = serial_obj.read_all()
    if serial_obj is None:
        con.close()
    return answer.decode()

four_point = True
rear = False
auto_zero = True
source_v = 1
source_delay = 10
num_pts = 50
trig_delay = 10
max_current = 1E-4
nPLC=1
auto_sense = True
setup = [
    # '*CLS',
         f'SYST:RSEN ' + ("ON" if four_point else "OFF"),
         'ROUT:TERM ' + ('REAR' if rear else 'FRON'),
         'SYST:AZER:STAT ' + ('ON' if auto_zero else 'OFF'),
         ]
source = [':SOUR:FUNC VOLT',
          'SOUR:CLE:AUTO ON',
          'SOUR:VOLT:MODE FIXED',
          f'SOUR:VOLT {source_v:.6E}',
          f'SOUR:DEL {source_delay/1000:.6E}',
          f'TRIG:COUN {num_pts:d}',
          f'TRIG:DEL {trig_delay/1000:.6E}'
          ]
sense = ['SENS:FUNC:CONC ON',
         'SENS:FUNC "CURR"',
         f'SENS:CURR:PROT {max_current:.6E}',
         f'SENS:CURR:NPLC {nPLC:.4f}',
         f'SENS:CURR:RANG:' + 'AUTO ON' if auto_sense else f' {max_current:.6E}',
         'FORM:ELEM CURR'
         ]

ser = serial.Serial(port='COM4')
ser.write(b'*RST\r\n')
# ser.read(1)
# ser.write(b'*IDN?\r')
# for _ in range(50):
#     time.sleep(0.001)
#     print(ser.in_waiting)
# print(ser.read(ser.in_waiting))
# print(ask('SYST:ERR?', serial_config, serial_obj=ser))
# for _ in range(10):
ser.write(b'STAT:QUE:CLEAR\r')#, serial_obj=ser))
error = ask('SYST:ERR?', serial_obj=ser, serial_config=serial_config)
print(f"Error\t\t: {error}")

for command in setup + source+sense:
    print(f"Sending: {repr(command+eol)}")
    ser.write((command+eol).encode())
    # time.sleep(0.2)
    # error = safe_ask('SYST:ERR?', serial_obj=ser)#, serial_config=serial_config)
    # print(f"Error\t\t: {error}")
    # time.sleep(0.1)
time.sleep(0.5)
ser.write(b'INIT\r')
start = time.time()
print(safe_ask('FETC?\r',serial_obj=ser))#, serial_config=serial_config))
end = time.time()
print(f"This took {end-start:.3f}s")
# time.sleep(0.1)
# print(ser.read_all())
# for _ in range(30):
#     time.sleep(0.1)
#     print(safe_ask('*STB?', serial_obj=ser))
# config_str = eol.join(setup+source+sense)
# print(repr(config_str))
# print(f'Config string ready: {config_str}')
# ser.write(config_str.encode())  # Don't need a response
# time.sleep(0.05)  # Short delay after configuring to allow Keithley to get ready.
# print(safe_ask('SYST:ERR?', serial_obj=ser))