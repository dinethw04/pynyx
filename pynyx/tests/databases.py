from sqlalchemy import create_engine
import urllib
import pandas as pd
import numpy as np
from pynyx import data

# engine = create_engine(r'mssql+pyodbc://SIVALABSER-001\\SQLEXPRESS/CQD')
# con = engine.connect()
raw_data = np.arange(0, 10, 0.5)
volts, currs = raw_data[::2], raw_data[1::2]  # Data is in format of Volt, Current, Volt, Current etc.
print(volts, currs)
uid = data.create_unique_id()
times = [uid for _ in volts]
format_data = np.column_stack([volts, currs])
df2 = pd.DataFrame()

df2['Voltage_V'] = volts
df2['Current_A'] = currs

print(df2)

data.save_iv_files(sample={'name': 'test'}, iv_data=df2, metadata={'flavor': 'chocolate'}, single_file='zip')
# df = pd.DataFrame(format_data, columns=['Voltage_(V)', 'Current_(A)'])
# df['Unique_ID'] = uid
# df.to_sql(name='SED_IV_Raw', con=engine)
# sample = data.get_sample_db(QDIP_name ='120922A', simple=True)
# print(df)
