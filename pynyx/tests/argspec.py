import inspect
othervar = 1
def best_func(start=1, stop=-1, step=0.1):
    vars = locals()
    print(f"I'm the best because i start at {start}, stop at {stop} and step with {step}")
    return vars

last_call = best_func(step=4)
print(last_call)
print(best_func(**last_call))
# kwargs = inspect.signature(best_func)
# print(kwargs, type(kwargs))