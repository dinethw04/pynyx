"""
This module controls automated measurements for sets of equipment for specific tests.
"""
import time, datetime
from pynyx.equipment import Keithley2400, CI_SR200, Lakeshore
from pynyx import setup_logger, data
logger = setup_logger(__name__)

class Measurement(object):
    """
    Base class for automating full measurements of attributes from a sample
    """
    def __init__(self, sample, out_path=None, config_path=None, recovery_path=None, **kwargs):
        """
        Sets up the measurement. The base class only sets some paths for recovery and setup.
        :param sample: Dictionary of the sample data for this measurement
        :param config_path: Path to the config file, if None its the caller's path
        :param recovery_path: Path to the recovery directory, if None its pynyx's path
        :param kwargs: Optional keywords.
        """
        self.sample = sample
        # TODO handle default cases
        self.out_path = out_path
        self.config_path = config_path
        self.recovery_path = recovery_path
        self.status = 'Initializing'

    def setup(self):
        """
        Sets up all instruments using their configurations
        :return:
        """

    def run(self):
        """
        This method runs the measurement, does all the cool stuff and makes sure we can recover
        Make sure this method is overwritten
        :return:
        """
        raise NotImplementedError

    def save_state(self):
        """
        This method saves the state of the measurement, such that we can recover from a failed one.
        :return:
        """
        raise NotImplementedError

    def recover(self, recovery_path=None):
        """
        This method recovers the measurement from the last saved state.
        :param recovery_path: Path to the folder containing the recovery data
        :return:
        """
        raise NotImplementedError


class Radiometric(Measurement):
    """
    This is a measurement of the radiometric properties of a optoelectric sensor.
    This eventually returns data like the QE, D*, NEDT, and so forth.
    """
    def __init__(self, sample, bb_temps, dewar_temps, bandpass_filters, apertures_dict,
                 smu=None, iv_config=None,
                 blackbody=None, blackbody_config=None,
                 temp_control=None, temp_control_config=None,**kwargs):
        """
        Sets up a radiometric measurement.
        :param bb_temp_list: list of blackbody temperatures to meausre. Must be at least 2
        :param smu: Keithley object to perform source-measure measurements
        :param iv_config: config dictionary for the Keithley
        :param blackbody: SerialIntrument object which is a blackbody controller
        :param blackbody_config: Config dictionary for the blackbody
        :param kwargs: other kwargs for the base class
        """
        super().__init__(sample, **kwargs)
        self.bb_temps = bb_temps
        self.dewar_temps = dewar_temps
        self.bandpass_filters = bandpass_filters
        self.apertures_dict = apertures_dict
        if smu is None:
            smu = Keithley2400()
        self.smu = smu
        if iv_config is not None:
            self.smu.config_iv(**iv_config)
        if blackbody is None:
            blackbody = CI_SR200()
        self.blackbody = blackbody
        if blackbody_config is not None:
            self.blackbody.config_tol(**blackbody_config)
        if temp_control is None:
            temp_control = Lakeshore()
        self.temp_control = temp_control
        self.equipment = [smu, blackbody]

    def run(self):
        self.status = 'Starting measurement'
        logger.debug(self.status)
        metadata = {'group_id' : data.get_group_id()}
        for bb_temp in self.bb_temps:
            logger.debug(f"Setting blackbody temp to {bb_temp}")
            self.blackbody.temp = bb_temp
            for dewar_temp in self.dewar_temps:
                logger.debug(f"Setting Dewar temp to {dewar_temp}")
                self.temp_control.temp = dewar_temp
                for bpf in self.bandpass_filters:
                    logger.debug(f"Setting bandpass filter to {bpf}")
                    self.blackbody.filter = bpf
                    for aperture in self.apertures_dict[bpf]:
                        logger.debug(f"Setting aperture to {aperture}")
                        self.blackbody.aperture = aperture
                        # Check for everything to be ready
                        self.status = 'Waiting for temps to stabilize'
                        logger.debug(self.status)
                        while not self.blackbody.is_stable() and not self.temp_control.is_stable():
                            self.status = f"Waiting for temps to stabilize. Blackbody: {self.blackbody.temp}, Dewar: {self.temp_control.temp}"
                            time.sleep(1)
                        ivt_df = self.smu.sweep_i_vs_t()
                        metadata['date-time'] = datetime.datetime.now()
                        metadata['Sweep_ID'] = 'S' + data.create_unique_id()
                        metadata['BB_set_temp_(C)'] = bb_temp
                        metadata['BB_read_temp_(C)'] = self.blackbody.temp
                        metadata['Dewar_set_temp_(C)'] = dewar_temp
                        metadata['Dewar_read_temp_(C)'] = self.temp_control.temp
                        metadata['Bandpass_filter'] = bpf
                        metadata['Aperture'] = aperture
                        data.save_ivt_files(self.sample, ivt_df, metadata, self.out_path, single_file='zip')
