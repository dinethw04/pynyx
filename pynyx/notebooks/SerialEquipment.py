import numpy as np
import time
import serial

class SerialInstrument:
    def __init__(self,comPort,eol='\r\n',**kwargs):
        self.serialkwargs = kwargs
        self.eol=eol
        self.com = comPort
    def __enter__(self):
        return self
    def __exit__(self, type, value, traceback):
        pass

    def __readline(self, con, eol='\r\n'):
        answer = bytearray()
        while True:
            c=con.read(1)
            if c not in eol.encode():
                answer+=c
            else:
                break
        return answer

    def __read(self,con):
        return self.__readline(con,eol=self.eol)
    
    def __readall(self,con):
        answer = bytearray()
        while True:
            if con.in_waiting:
                c=con.read(con.in_waiting)
            else:
                c=con.read(1)
            if c:
                answer+=c
            else:
                break
        return answer

    def __readall2(self,con,delay=0):
        answer = bytearray()
        answer += con.read(1)
        time.sleep(.1)
        while con.in_waiting:
            answer+=con.read(con.in_waiting)
            time.sleep(delay)
        return answer
    
    def write(self,msg):
        with serial.Serial(self.com,**self.serialkwargs) as con:
            con.write((msg+self.eol).encode())

    def writeline(self,msg,eol='\n'):
        with serial.Serial(self.com,**self.serialkwargs) as con:
            con.write((msg+eol).encode())
            
    def ask(self,msg):
        with serial.Serial(self.com,**self.serialkwargs) as con:
            con.write((msg+self.eol).encode())
            answer = self.__read(con)
        return ''.join([chr(i) for i in answer.rstrip()])
    
    def poll(self,msg,delay=0):
        with serial.Serial(self.com,**self.serialkwargs) as con:
            con.write((msg+self.eol).encode())
            answer = self.__readall2(con,delay=delay)
        return ''.join([chr(i) for i in answer.rstrip()])
    
    def poll_RAW(self,msg,delay=0):
        with serial.Serial(self.com,**self.serialkwargs) as con:
            con.write((msg+self.eol).encode())
            answer = self.__readall2(con,delay=delay)
        return answer
    
    def askline(self,msg,eol='\r\n'):
        with serial.Serial(self.com,**self.serialkwargs) as con:
            con.write((msg+eol).encode())
            answer = self.__readline(con,eol=eol)
        return ''.join([chr(i) for i in answer.rstrip()])
    


class Keithley2400:
    def __init__(self,comPort,**kwargs):
        self.eol='\r'
        self.kwargs = {'timeout':1,'baudrate':9600,
                  'parity':serial.PARITY_NONE,
                  'bytesize':serial.EIGHTBITS,
                  'eol':self.eol}
        self.kwargs.update(kwargs)
        self.con = SerialInstrument(comPort,**self.kwargs)

    def IV_Sweep_Config(self,start=-1,stop=1,
                 step=0.025,src_delay=9E-3,trg_delay=0,
                 count=100,PROT=1e-3,NPLC=1,port='REAR',
                 RANG_AUTO= 'ON',SWEP_SPACE='LIN'):

        config = ['*RST',
                  '*ESE 1',
                  '*SRE 32',
                  '*CLS',
                  'SYST:RSEN OFF',
                  'ROUT:TERM %s'%port,
                  'SYST:AZER:STAT ON',
                  'SENS:FUNC:CONC ON',
                  'SENS:FUNC CURR',
                  'SENS:CURR:PROT %s'%PROT,
                  'SENS:CURR:NPLC %s'%NPLC,
                  'SENS:CURR:RANG:AUTO %s'%RANG_AUTO,
                  'SOUR:FUNC VOLT',
                  'SOUR:CLE:AUTO ON',
                  'SOUR:VOLT:MODE SWE',
                  'SOURce:SWEep:SPAC %s'%SWEP_SPACE,
                  'SOUR:VOLT:START %s'%start,
                  'SOUR:VOLT:STOP %s'%stop,
                  'SOUR:VOLT:STEP %s'%step,
                  'SOUR:DEL %s'%src_delay,
                  'TRIG:COUN %s'%count,
                  'TRIG:DEL %s'%trg_delay,
                  'FORM:ELEM VOLT, CURR',
                  'READ?']
        return self.con.ask(self.eol.join(config))

    def keithleyDiode(self):
        config=['*RST',
                ':SENS:FUNC:CONC OFF',
                ':SOUR:FUNC CURR',
                ":SENS:FUNC ‘VOLT:DC’",
                ':SENS:VOLT:PROT 1',
                ':SOUR:CURR:START 1E-3',
                ':SOUR:CURR:STOP 10E-3',
                ':SOUR:CURR:STEP 1E-3',
                ':SOUR:CURR:MODE SWE',
                ':SOUR:SWE:RANG AUTO',
                ':SOUR:SWE:SPAC LIN',
                ':TRIG:COUN 10',
                ':SOUR:DEL 0.1',
                ':OUTP ON',
                ':READ?']
        return self.con.poll(self.eol.join(config))
        
    def I_Sense_V_Source_Configure(self):
        config = ['*RST',
                  'TRIG:SOUR IMM',
                  'SENS:FUNC:CONC OFF',
                  'SENS:FUNC "CURR"',
                  'SOUR:FUNC VOLT',
                  'SOUR:CLE:AUTO Off',
                  'TRIG:COUN 10',
                  'TRIG:DEL 0',
                  'SOUR:DEL 0.001',
                  'ROUT:TERM REAR'
                  'SYST:AZER:STAT ON',
                  'SENS:CURR:PROT 20.000E-3',
                  'SENS:CURR:NPLC 1.000',
                  'SYST:RSEN Off',
                  'SENS:CURR:RANG:AUTO On']
        
        self.con.write(self.eol.join(config))
        return self.eol.join(config)
    
    def errorDump(self):
        errlist=[]
        while 1:
            err = self.con.poll('SYST:ERR?')
            if 'No error' in err:
                break
            errlist.append(err)
        return errlist
    
    def ask(self,msg):
        return(self.con.ask(msg))
    def askline(self,msg,eol='\r'):
        return(self.con.ask(msg,eol=eol))
    def write(self,msg):
        return(self.con.write(msg))
    def writeline(self,msg,eol='\r'):
        return(self.con.writeline(msg,eol=eol))
    def poll(self,msg,delay=0):
        return(self.con.poll(msg,delay=delay))
        
class Lakeshore:
    def __init__(self, comPort,**kwargs):
        self.kwargs = {'baudrate':9600,
                  'parity':serial.PARITY_ODD,
                  'stopbits':serial.STOPBITS_ONE,
                  'bytesize':serial.SEVENBITS,
                  'timeout':1}
        self.kwargs.update(kwargs)
        self.con = SerialInstrument(comPort,**self.kwargs)

        self.commanDic={'QCLS':'Clear Interface Command',
                        'QESE':'Event Status Enabled',
                        'QESE?':'Event Status Enabled Query',
                        'QESR?':'Event Status Register Query',
                        'QIDN?':'Identification Query',
                        'QOPC':'Operation Complete Command',
                        'QOPC?':'Operation Complete Query',
                        'QRST':'Reset Instrument Command',
                        'QSRE':'Service Request Enable command',
                        'QSRE?':'Service Request Enable Query',
                        'QSTB?':'Status Byte Query',
                        'QTST?':'Self-Test Query',
                        'QWAI':'Wait-To-Continue Command',
                        'CMODE':'Control Loop Mode Command',
                        'CMODE?':'Control Lop Mode Query',
                        'CRDG?':'Celcius Reading',
                        'CRVDEL':'Delete User Curve Command',
                        'CRVHDR':'Curve Header Command',
                        'CRVHDR?':'Curve Header Query',
                        'CRVPT':'Curve Data Point Command',
                        'CRVPT?': 'Curve Data Point Query',
                        'CSET':'Control Loop Parameter Command',
                        'CSET?': 'Control Loop Parameter Query',
                        'DFLT':'Factory Defaults Command',
                        'DISPFLD': 'Display Field Command',
                        'DISPFLD?': 'Display Field Query',
                        'FILTER':'Input Filter Parameter Command',
                        'FILTER?': 'Input Filter Parameter Query',
                        'HTR?':'Heater Output Query',
                        'HTRRES':'Heater Resistance Setting Command',
                        'HTRRES?':'Heater Resistance Setting Query',
                        'IEEE':'IEEE Interface Parameter Command',
                        'IEEE?': 'IEEE Interface Parameter Query',
                        'INCRV':'Input Curve Number Command',
                        'INCRV?': 'Input Curve Number Query',
                        'INTYPE':'Input Type Parameter Command',
                        'INTYPE?': 'Input Type Parameter Query',
                        'KEYST?':'Keypad Status Query',
                        'KRDG?':'Kelvin Reading Query',
                        'LOCK':'Front Panel Keyboard Lock Command',
                        'LOCK?': 'Front Panel Keyboard Lock Query',
                        'MODE':'Set Local/Remote Mode',
                        'MODE?': 'Query Local/Remote Mode',
                        'PID':'Control Loop PID Values Command',
                        'PID?': 'Control Loop PID Values Query',
                        'RAMP':'Control Loop Ramp Command',
                        'RAMP?': 'Control Loop Ramp Query',
                        'RAMPST?':'Control Loop Ramp Status Query',
                        'REV?':'Input Firmware Revision Query',
                        'SCAL':'Generate SoftCal Curve Command',
                        'SETP':'Control Loop Set Point Command',
                        'SETP?': 'Control Loop Set Point Query',
                        'SRDG?':'Sensor Unit Reading Query',
                        'TEMP?':'Room-Temperature Comp. Temp. Query',
                        'TLIMIT':'Temperature Limit Command',
                        'TLIMIT?':'Temperature Limit Query',
                        'TUNEST?':'Control Loop 1 Tuning Query',
                        'Zone':'Control Loop ZOne Table Command',
                        'Zone?': 'Control Loop ZOne Table Query'
                        }



    def getTemp(self):
        return self.con.ask('KRDG?')

    def getTempSetPoint(self, loop):
        return self.con.ask('SETP? %s' % loop)

    def setTemp(self, loop, temp):
        self.con.write('SETP %s, %0.3f' % (loop, temp))

    def setTempAndWait(self, loop, temp,percent = .01):
        self.con.write('SETP %s, %0.3f' % (loop, temp))
        i = 0
        while 1:
            try:
                ct = np.array([float(self.con.ask('KRDG?')) for i in range(10)])
                meanT = np.mean(ct)
                stdT  = np.std(ct)

                if abs(meanT-temp)<percent*temp and (stdT < percent*temp):
                    return 'stable: %s' % meanT
                time.sleep(.2)
            except:
                i+=1
                if i >5:
                    return 'error: could not read temp'

    def isStable(self):
        temp = self.con.ask('KRDG?')
        i = 0
        for i in range(3):
            ct = self.con.ask('KRDG?')
            if ct == temp:
                i += 1
        time.sleep(.2)
        if i == 3:
            return True
        else:
            return False

    def arbitraryCommand(self,cmd):
        return self.con.ask(cmd)
    def cmd(self,cmd):
        return self.con.ask(cmd)


class CI_SR800R:
    def __init__(self, comPort,**kwargs):
        self.kwargs = {'timeout':1}
        self.kwargs.update(kwargs)
        self.con = SerialInstrument(comPort,**self.kwargs)


    def getTemp(self):
        return self.con.ask('GETTEMPERATURE')

    def getTempSetPoint(self):
        return self.con.ask('GETABSOLUTESETPOINT')

    def setTemp(self, temp):
        self.con.write('SETTEMPERATURE %0.3f' % temp)

    def setTempAndWait(self, temp):
        self.con.write('SETTEMPERATURE %0.3f' % temp)
        while 1:
            r = self.con.query('ISTEMPERATURESTABLE')
            if r == '1':
                t = self.getTemp()
                return 'stable: %s' % t

    def isStable(self):
        return self.con.query('ISTEMPERATURESTABLE')


class CI_SR200:
    def __init__(self, comPort,**kwargs):
        self.kwargs = {'timeout':1}
        self.kwargs.update(kwargs)
        self.con = SerialInstrument(comPort,**self.kwargs)

        self.commandList=['GETAXISSTATUS',
                          'AXISHOMING',
                          'SETTARGET',
                          'SA',
                          'SETACTIVEAXIS',
                          'GETACTIVEAXIS',
                          'GETNUMBEROFAXES',
                          'RA',
                          'GETTARGETFREQUENCY',
                          'GETAXISEFL',
                          'GETTEMPERATURE',
                          'GETTEMPERATURERANGE',
                          'RG',
                          'ISTEMPERATURESTABLE',
                          'RE',
                          'SETTEMPERATURE',
                          'ST',
                          'GETINPUTSTATUS',
                          'OUTPUTENABLE',
                          'SETOUTPUT',
                          'ECHO',
                          'SETPRESET',
                          'GETMAXPRESETCOUNT',
                          '*IDN?',
                          'RV',
                          'GETBITERROR',
                          'GETSYSTEMINFO',
                          'CALIBRATETOUCHPANEL',
                          'SETREMOTETYPE',
                          'GETABSOLUTESETPOINT',
                          'SETCOMUPTERNAME',
                          'DM']
    def getTemp(self):
        return self.con.ask('GETTEMPERATURE')

    def getTempSetPoint(self):
        return self.con.ask('GETABSOLUTESETPOINT')

    def setTemp(self, temp):
        self.con.write('SETTEMPERATURE %d' % int(temp))

    def setTempAndWait(self, temp):
        self.con.write('SETTEMPERATURE %d' % int(temp))
        while 1:
            r = self.con.ask('ISTEMPERATURESTABLE')
            if r == '1':
                t = self.getTemp()
                return 'stable: %s' % t

    def isStable(self):
        return self.con.ask('ISTEMPERATURESTABLE')
    
    def getApertureList(self):
        self.con.ask('SETACTIVEAXIS 1')
        return self.con.ask('GETTARGETLIST')
        
    def getFilterList(self):
        self.con.ask('SETACTIVEAXIS 2')
        return self.con.ask('GETTARGETLIST')
    
    def setAperture(self,n):
        print('moving to aperture: %s' %n)
        self.con.ask('SETACTIVEAXIS 1')
        self.con.ask('SETTARGET %d' %n)
        while self.con.ask('GETAXISSTATUS') != '0':
            print('moving...')
        return self.con.ask('RA')
    
    def setFilter(self,n):
        print('moving to filter: %s' %n)
        self.con.ask('SETACTIVEAXIS 2')
        self.con.ask('SETTARGET %d' %n)
        while self.con.ask('GETAXISSTATUS') != '0':
            print('moving...')
        return self.con.ask('RA')
    
    def arbitraryCommand(self,cmd):
        return self.con.ask(cmd)
    def cmd(self,cmd):
        return self.con.ask(cmd)
