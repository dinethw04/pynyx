import time
import visa


class Lakeshore:
    def __init__(self, instrument=None,bus='GPIB', address=1):
        if instrument is None:
            self.BB = visa.instrument('%s::%i' %(bus,address))
        else:
            self.BB = instrument

        self.commanDic={'QCLS':'Clear Interface Command',
                        'QESE':'Event Status Enabled',
                        'QESE?':'Event Status Enabled Query',
                        'QESR?':'Event Status Register Query',
                        'QIDN?':'Identification Query',
                        'QOPC':'Operation Complete Command',
                        'QOPC?':'Operation Complete Query',
                        'QRST':'Reset Instrument Command',
                        'QSRE':'Service Request Enable command',
                        'QSRE?':'Service Request Enable Query',
                        'QSTB?':'Status Byte Query',
                        'QTST?':'Self-Test Query',
                        'QWAI':'Wait-To-Continue Command',
                        'CMODE':'Control Loop Mode Command',
                        'CMODE?':'Control Lop Mode Query',
                        'CRDG?':'Celcius Reading',
                        'CRVDEL':'Delete User Curve Command',
                        'CRVHDR':'Curve Header Command',
                        'CRVHDR?':'Curve Header Query',
                        'CRVPT':'Curve Data Point Command',
                        'CRVPT?': 'Curve Data Point Query',
                        'CSET':'Control Loop Parameter Command',
                        'CSET?': 'Control Loop Parameter Query',
                        'DFLT':'Factory Defaults Command',
                        'DISPFLD': 'Display Field Command',
                        'DISPFLD?': 'Display Field Query',
                        'FILTER':'Input Filter Parameter Command',
                        'FILTER?': 'Input Filter Parameter Query',
                        'HTR?':'Heater Output Query',
                        'HTRRES':'Heater Resistance Setting Command',
                        'HTRRES?':'Heater Resistance Setting Query',
                        'IEEE':'IEEE Interface Parameter Command',
                        'IEEE?': 'IEEE Interface Parameter Query',
                        'INCRV':'Input Curve Number Command',
                        'INCRV?': 'Input Curve Number Query',
                        'INTYPE':'Input Type Parameter Command',
                        'INTYPE?': 'Input Type Parameter Query',
                        'KEYST?':'Keypad Status Query',
                        'KRDG?':'Kelvin Reading Query',
                        'LOCK':'Front Panel Keyboard Lock Command',
                        'LOCK?': 'Front Panel Keyboard Lock Query',
                        'MODE':'Set Local/Remote Mode',
                        'MODE?': 'Query Local/Remote Mode',
                        'PID':'Control Loop PID Values Command',
                        'PID?': 'Control Loop PID Values Query',
                        'RAMP':'Control Loop Ramp Command',
                        'RAMP?': 'Control Loop Ramp Query',
                        'RAMPST?':'Control Loop Ramp Status Query',
                        'REV?':'Input Firmware Revision Query',
                        'SCAL':'Generate SoftCal Curve Command',
                        'SETP':'Control Loop Set Point Command',
                        'SETP?': 'Control Loop Set Point Query',
                        'SRDG?':'Sensor Unit Reading Query',
                        'TEMP?':'Room-Temperature Comp. Temp. Query',
                        'TLIMIT':'Temperature Limit Command',
                        'TLIMIT?':'Temperature Limit Query',
                        'TUNEST?':'Control Loop 1 Tuning Query',
                        'Zone':'Control Loop ZOne Table Command',
                        'Zone?': 'Control Loop ZOne Table Query'
                        }



    def getTemp(self):
        return self.BB.query('TEMP?')

    def getTempSetPoint(self, loop):
        return self.BB.query('SETP? %s' % loop)

    def setTemp(self, loop, temp):
        self.BB.write('SETP %s, %0.3f' % (loop, temp))

    def setTempAndWait(self, loop, temp):
        self.BB.write('SETP %s, %0.3f' % (loop, temp))
        i = 0
        while 1:
            ct = self.BB.query('TEMP?')
            if ct == temp:
                i += 1
            else:
                i = 0
            if i == 3:
                return 'stable: %s' % ct
            time.sleep(.2)

    def isStable(self):
        temp = self.BB.query('TEMP?')
        i = 0
        for i in range(3):
            ct = self.BB.query('TEMP?')
            if ct == temp:
                i += 1
        time.sleep(.2)
        if i == 3:
            return True
        else:
            return False

    def arbitraryCommand(self,cmd):
        return self.BB.query(cmd)
    def cmd(self,cmd):
        return self.BB.query(cmd)


class CI_SR800R:
    def __init__(self,instrument=None,bus='GPIB', address=1):
        if instrument is None:
            self.BB = visa.instrument('%s::%i' %(bus,address))
        else:
            self.BB = instrument

    def getTemp(self):
        return self.BB.query('GETTEMPERATURE')

    def getTempSetPoint(self):
        return self.BB.query('GETABSOLUTESETPOINT')

    def setTemp(self, temp):
        self.BB.write('SETTEMPERATURE %0.3f' % temp)

    def setTempAndWait(self, temp):
        self.BB.write('SETTEMPERATURE %0.3f' % temp)
        while 1:
            r = self.BB.query('ISTEMPERATURESTABLE')
            if r == '1':
                t = self.getTemp()
                return 'stable: %s' % t

    def isStable(self):
        return self.BB.query('ISTEMPERATURESTABLE')


class CI_SR200:
    def __init__(self,instrument=None,bus='GPIB', address=1):
        if instrument is None:
            self.BB = visa.instrument('%s::%i' %(bus,address))
        else:
            self.BB = instrument
        self.commandList=['GETAXISSTATUS',
                          'AXISHOMING',
                          'SETTARGET',
                          'SA',
                          'SETACTIVEAXIS',
                          'GETACTIVEAXIS',
                          'GETNUMBEROFAXES',
                          'RA',
                          'GETTARGETFREQUENCY',
                          'GETAXISEFL',
                          'GETTEMPERATURE',
                          'GETTEMPERATURERANGE',
                          'RG',
                          'ISTEMPERATURESTABLE',
                          'RE',
                          'SETTEMPERATURE',
                          'ST',
                          'GETINPUTSTATUS',
                          'OUTPUTENABLE',
                          'SETOUTPUT',
                          'ECHO',
                          'SETPRESET',
                          'GETMAXPRESETCOUNT',
                          '*IDN?',
                          'RV',
                          'GETBITERROR',
                          'GETSYSTEMINFO',
                          'CALIBRATETOUCHPANEL',
                          'SETREMOTETYPE',
                          'GETABSOLUTESETPOINT',
                          'SETCOMUPTERNAME',
                          'DM']
    def getTemp(self):
        return self.BB.query('GETTEMPERATURE')

    def getTempSetPoint(self):
        return self.BB.query('GETABSOLUTESETPOINT')

    def setTemp(self, temp):
        self.BB.write('SETTEMPERATURE %0.3f' % temp)

    def setTempAndWait(self, temp):
        self.BB.write('SETTEMPERATURE %0.3f' % temp)
        while 1:
            r = self.BB.query('ISTEMPERATURESTABLE')
            if r == '1':
                t = self.getTemp()
                return 'stable: %s' % t

    def isStable(self):
        return self.BB.query('ISTEMPERATURESTABLE')

    def arbitraryCommand(self,cmd):
        return self.BB.query(cmd)
    def cmd(self,cmd):
        return self.BB.query(cmd)
