
import time
import serial
import numpy as np
import pandas as pd
from pynyx import setup_logger, data
logger = setup_logger(__name__)


class SerialInstrument(object):
    """
    Base class for instruments.
    This class implements the low level serial calls in safe loops and has some placeholder methods for the
    inhereting classes
    """
    def __init__(self, com_port, eol='\r\n', serial_config=None, **kwargs):
        """
        Creates the serial instrument. Base class just has serial config stuff.
        :param com_port:
        :param eol:
        :param serial_config:
        :param kwargs:
        """
        # TODO, should have a Serial object that we reference throughout instead of open/close each time.
        self.com = com_port
        self.eol = eol
        if serial_config is None:
            serial_config = {
                # 'port' : com_port,
                'timeout': 10, 'baudrate': 9600,
                       'parity': serial.PARITY_NONE,
                       'bytesize': serial.EIGHTBITS,
                       #'eol': eol
                       }
        serial_config.update(kwargs)
        #self.serial_config = 1
        self.serial_config = serial_config
        self.serial_port = serial.Serial(port=com_port, **serial_config)
        self.serial_port.close()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass

    def __readline(self, con, eol='\r\n'):
        answer = bytearray()
        while True:
            c = con.read(1)
            if c not in eol.encode():
                answer += c
            else:
                break
        return answer

    def __read(self, con):
        return self.__readline(con, eol=self.eol)

    def __readall(self, con):
        answer = bytearray()
        while True:
            if con.in_waiting:
                c = con.read(con.in_waiting)
            else:
                c = con.read(1)
            if c:
                answer += c
            else:
                break
        return answer

    def __readall2(self, con, delay=0.05):
        answer = bytearray()
        answer += con.read(1)
        time.sleep(.1)
        while con.in_waiting:
            answer += con.read(con.in_waiting)
            time.sleep(delay)
        return answer

    def risky_write(self, msg):
        with serial.Serial(self.com, **self.serial_config) as con:
            logger.debug(f"Now writing to {self.com}: {repr((msg + self.eol).encode())}")
            con.write((msg + self.eol).encode())

    def writeline(self, msg, eol='\n'):
        with serial.Serial(self.com, **self.serial_config) as con:
            logger.debug(f"Now writing line to {self.com}:\n{msg}")
            con.write((msg + eol).encode())

    def risky_ask(self, msg):
        with serial.Serial(self.com, **self.serial_config) as con:
            logger.debug(f"On {self.com} now asking :\n{msg}")
            con.write((msg + self.eol).encode())
            answer = self.__read(con)
        return ''.join([chr(i) for i in answer.rstrip()])

    def ask(self, msg, auto_close=True, delay=0.05, timeout=20):
        if not self.serial_port.is_open:
            self.serial_port.open()
        logger.debug(f"About to safely ask: {repr((msg + self.eol).encode())}")
        self.serial_port.write((msg + self.eol).encode())
        time.sleep(0.25)
        last_bytes = [-1, -1, -1]
        # Wait for message to arrive
        for i in range(int(timeout / delay)):
            if self.serial_port.in_waiting and all(last == self.serial_port.in_waiting for last in last_bytes):
                break
            else:
                last_bytes[i % len(last_bytes)] = self.serial_port.in_waiting
                print(f"We have {self.serial_port.in_waiting} bytes...")
                time.sleep(delay)
        answer = self.serial_port.read_all()
        if auto_close:
            self.serial_port.close()
        logger.debug(f"Return is {answer!s}")
        return answer.decode()

    def safe_ask(self, *args, **kwargs):
        return self.ask(*args, **kwargs)

    def write(self, msg, auto_close=True, delay=0.05, timeout=20):
        if not self.serial_port.is_open:
            self.serial_port.open()
        logger.debug(f"About to safely write: {repr((msg + self.eol).encode())}")
        self.serial_port.write((msg + self.eol).encode())
        time.sleep(0.25)
        if auto_close:
            self.serial_port.close()
        return True

    def poll(self, msg, delay=0):
        with serial.Serial(self.com, **self.serial_config) as con:
            logger.debug(f"On {self.com} now polling :\n{msg}")
            con.write((msg + self.eol).encode())
            answer = self.__readall2(con, delay=delay)
        return ''.join([chr(i) for i in answer.rstrip()])

    def poll_RAW(self, msg, delay=0):
        with serial.Serial(self.com, **self.serial_config) as con:
            con.write((msg + self.eol).encode())
            answer = self.__readall2(con, delay=delay)
        return answer

    def askline(self, msg, eol='\r\n'):
        with serial.Serial(self.com, **self.serial_config) as con:
            con.write((msg + eol).encode())
            answer = self.__readline(con, eol=eol)
        return ''.join([chr(i) for i in answer.rstrip()])

    def check_ready(self):
        """
        Determines if the instrument is ready to recieve instruction, and that communication is working.
        :return:
        """
        raise NotImplementedError

    def setup(self):
        """
        Runs any initial configuration needed before it is ready
        :return:
        """
        raise NotImplementedError


class Keithley2400(SerialInstrument):
    """
    Class for a Keithley 2400 source-measure unit.
    """
    def __init__(self, com_port='COM4', config_method=None, **kwargs):
        """
        Calls constructor for base class, and sets a default configuration method, ivt, iv, or i_vs_t
        :param com_port:
        :param config_method:
        :param kwargs:
        """
        super().__init__(com_port, **kwargs)
        if config_method is None:
            self.config_method = self.config_sweep_iv  # Default configure method
        else:
            self.config_method = config_method
        self.config_kwargs = {}  # Kwargs for that method. Allows recovery of the state from a pickled object
        self.is_configured = False

    def config_sweep_iv(self, start=-1, stop=1,
                        step=0.025, src_delay=9E-3, trg_delay=0,
                        count=100, PROT=1e-3, NPLC=1, port='REAR',
                        RANG_AUTO='ON', SWEP_SPACE='LIN'):
        """
        Configures the keithley to run a I(V) sweep once 'init' is sent.
        :param start:
        :param stop:
        :param step:
        :param src_delay:
        :param trg_delay:
        :param count:
        :param PROT:
        :param NPLC:
        :param port:
        :param RANG_AUTO:
        :param SWEP_SPACE:
        :return:
        """
        self.config_kwargs = locals()  # Capture all of the passed variables as a dictionary. Store in the object
        self.config_method = self.config_sweep_iv  # Capture the method that we need to call next time.
        # TODO find clever way of having function signature and storing generic kwargs
        # default = {start=-1, stop=1,
        #                 step=0.025, src_delay=9E-3, trg_delay=0,
        #                 count=100, PROT=1e-3, NPLC=1, port='REAR',
        #                 RANG_AUTO='ON', SWEP_SPACE='LIN'}
        # self.config_kwargs = default.update(kwargs)
        config = ['*ESE 1',
                  '*SRE 32',
                  '*CLS',
                  'SYST:RSEN OFF',
                  'ROUT:TERM %s' % port,
                  'SYST:AZER:STAT ON',
                  'SENS:FUNC:CONC ON',
                  'SENS:FUNC CURR',
                  'SENS:CURR:PROT %s' % PROT,
                  'SENS:CURR:NPLC %s' % NPLC,
                  'SENS:CURR:RANG:AUTO %s' % RANG_AUTO,
                  'SOUR:FUNC VOLT',
                  'SOUR:CLE:AUTO ON',
                  'SOUR:VOLT:MODE SWE',
                  'SOURce:SWEep:SPAC %s' % SWEP_SPACE,
                  'SOUR:VOLT:START %s' % start,
                  'SOUR:VOLT:STOP %s' % stop,
                  'SOUR:VOLT:STEP %s' % step,
                  'SOUR:DEL %s' % src_delay,
                  'TRIG:COUN %s' % count,
                  'TRIG:DEL %s' % trg_delay,
                  'FORM:ELEM VOLT, CURR',
                  'READ?']
        self.is_configured = True
        return self.ask(self.eol.join(config))

    def keithleyDiode(self):
        config = ['*RST',
                  ':SENS:FUNC:CONC OFF',
                  ':SOUR:FUNC CURR',
                  ":SENS:FUNC ‘VOLT:DC’",
                  ':SENS:VOLT:PROT 1',
                  ':SOUR:CURR:START 1E-3',
                  ':SOUR:CURR:STOP 10E-3',
                  ':SOUR:CURR:STEP 1E-3',
                  ':SOUR:CURR:MODE SWE',
                  ':SOUR:SWE:RANG AUTO',
                  ':SOUR:SWE:SPAC LIN',
                  ':TRIG:COUN 10',
                  ':SOUR:DEL 0.1',
                  ':OUTP ON',
                  ':READ?']
        return self.poll(self.eol.join(config))

    def config_isense_vsource(self, source_v=-2, num_pts=10, max_current=1E-4,
                              four_point=False, rear=False, auto_zero=True, auto_sense=True,
                              nPLC=1, trig_delay=25, source_delay=25):
        """
        Configures the keithley to sense current and source voltage when 'init' is called.
        :param source_v:
        :param num_pts:
        :param max_current:
        :param four_point:
        :param rear:
        :param auto_zero:
        :param auto_sense:
        :param nPLC:
        :param trig_delay: ms
        :param source_delay: ms
        :return:
        """
        self.config_kwargs = locals()  # Capture all of the passed variables as a dictionary. Store in the object
        self.config_method = self.config_isense_vsource  # Capture the method that we need to call next time.
        # TODO check value ranges and assert value error if out of range
        setup = [
                 f'SYST:RSEN '+ ("ON" if four_point else "OFF"),
                 'ROUT:TERM ' + ('REAR' if rear else 'FRON'),
                 'SYST:AZER:STAT ' + ('ON' if auto_zero else 'OFF'),
                 ]
        source = [':SOUR:FUNC VOLT',
                  'SOUR:CLE:AUTO ON',
                  'SOUR:VOLT:MODE FIXED',
                  f'SOUR:VOLT {source_v:.6E}',
                  f'SOUR:DEL {source_delay/1000:.6E}',
                  f'TRIG:COUN {num_pts:d}',
                  f'TRIG:DEL {trig_delay/1000:.6E}'
                  ]
        sense = ['SENS:FUNC:CONC ON',
                 'SENS:FUNC "CURR"',
                 f'SENS:CURR:PROT {max_current:.6E}',
                 f'SENS:CURR:NPLC {nPLC:.4f}',
                 f'SENS:CURR:RANG:' + ('AUTO ON' if auto_sense else f' {max_current:.6E}'),
                 'FORM:ELEM CURR'
                 ]
        logger.debug(f'Pasing list to make a config string: {setup!s}')
        config_str = self.eol.join(setup + source + sense)
        logger.debug(f'Config string ready: {repr(config_str)}')
        for cmds in [setup, source, sense]:
            self.write(self.eol.join(cmds))  # Don't need a response
            time.sleep(0.5)
        time.sleep(0.05)  # Short delay after configuring to allow Keithley to get ready.
        self.is_configured = True
        return config_str

    def clear_errors(self):
        self.write('STAT:QUE:CLEAR')

    def get_error(self):
        return self.ask('SYST:ERR?')

    def error_dump(self):
        errlist = []
        while True:
            err = self.poll('SYST:ERR?')
            if 'No error' in err:
                break
            errlist.append(err)
        return errlist

    def check_ready(self):
        logger.debug(f"Checking if Keithley is ready on com {self.com}")
        idn = self.ask('*IDN?')
        if 'keithley' in idn.casefold():
            return True
        else:
            logger.error("Could not get IDN string from Keithley!")
            return False

    def setup(self, config_method=None, **kwargs):
        logger.debug(f"Setting up Keithley on com {self.com}")
        self.reset()
        if config_method is None:
            config_method = self.config_method
        elif config_method == self.config_method and not kwargs:
            kwargs = self.config_kwargs
        elif config_method in ['iv', 'IV', 'i(v)', 'I(V)']:
            config_method = self.config_sweep_iv
        elif config_method in ['ivt', 'iv(t)', 'IVT', 'i(t)', 'I(t)']:
            config_method = self.config_isense_vsource
        return config_method(**kwargs)

    def reset(self):
        """
        Resets the Keithley to defaults
        :return:
        """
        logger.info("Resetting Keithley")
        self.write('*RST')
        time.sleep(0.05)

    def soak_voltage(self, soak_v=None, soak_time=1):
        self.setup()
        if soak_v is None:
            if 'source_v' in self.config_kwargs:
                soak_v = self.config_kwargs['source_v']
            elif 'start' in self.config_kwargs:
                soak_v = self.config_kwargs['start']
            else:
                soak_v = -1
        logger.debug(f"Setting Keithley to soak {soak_v:.3f}V for {time}s")
        soak = [f'SOUR:VOLT {soak_v}',
                'SOUR:CLE:AUTO OFF',
                'OUTP ON']
        self.write(self.eol.join(soak))
        time.sleep(soak_time)
        self.write('OUTP OFF' + self.eol)

    def take_measurement(self, timeout=20):
        """
        Once configured, this takes a measurement, waits for it to complete, and returns the data.
        :return:
        """
        logger.debug("Taking a measurement")
        if not self.is_configured:
            self.setup()
        self.write('INIT')
        logger.debug("Waiting for measurement to complete...")
        data_str = self.ask('FETCH?', timeout=timeout)
        data = np.array([float(val) for val in data_str.split(',')])
        logger.debug(f"Measurement complete, {len(data)} values returned")
        # TODO check if all data returned. Capture buffer overflow errors, etc.
        # TODO figure out what the data format is from the config_method_kwargs
        return data

    def sweep_iv(self, timeout=20, as_data_frame=True, **config_kwargs):
        """
        Sweeps IV once configured
        :return:
        """
        self.setup(config_method=self.config_sweep_iv, **config_kwargs)
        raw_data = self.take_measurement(timeout=timeout)
        volts, currs = raw_data[::2], raw_data[1::2]  # Data is in format of Volt, Current, Volt, Current etc.
        data = np.column_stack((volts, currs))
        if as_data_frame:
            return pd.DataFrame(data, columns=['Voltage_(V)', 'Current_(A)'])
        else:
            return data

    def sweep_i_vs_t(self, timeout=20, as_data_frame=True, **config_kwargs):
        if self.config_method == self.config_isense_vsource and self.config_kwargs == config_kwargs:
            logger.debug("Skipping setup, same entires")
        else:
            self.setup(config_method=self.config_isense_vsource, **config_kwargs)
        raw_data = self.take_measurement(timeout=timeout)
        times = np.linspace(0, self.config_kwargs['num_pts']*self.config_kwargs['trig_delay'],
                            self.config_kwargs['num_pts'])
        data = np.column_stack((times, raw_data))
        if as_data_frame:
            return pd.DataFrame(data, columns=['Time_(ms)', 'Current_(A)'])
            input('was dataframe')
        else:
            return data

    def sweep_ivt(self, start, stop, step, **config_kwargs):
        # TODO give api to call current status
        self.reset()
        logger.debug("Starting IV(t) sweep")
        id_int = int(data.create_unique_id()[2:])
        # all_data = {'Sweep_ID' : f"SID{id_int:d}"}
        all_dataframes = []
        volts = np.arange(start=start, stop=stop, step=step)
        for i, volt in enumerate(volts):
            # id_int += 1
            # TODO, lots of unneeeded reconfigs. Only need to set source volt each time.
            self.config_isense_vsource(source_v=volt, **config_kwargs)
            i_vs_t = self.sweep_i_vs_t()
            if 'Unique_ID' not in i_vs_t.columns:
                i_vs_t.insert(0, 'Unique_ID', f"ID{id_int+i:d}")
            all_dataframes.append(i_vs_t)
        ivt_data = pd.DataFrame({'Unique_ID': [f"ID{id_int + count:d}" for count in range(len(all_dataframes))],
                                 'Sweep_ID': [f"SID{id_int:d}" for _ in range(len(all_dataframes))],
                                 'Voltage_(V)': [v for v in volts],
                                 'Avg_Current_(A)': [df['Current_(A)'].mean() for df in all_dataframes],
                                 'rms_Current_(A)': [np.sqrt((df['Current_(A)'] ** 2).mean()) for df in all_dataframes],
                                 'rms_Noise_Current(A)': [df['Current_(A)'].std() for df in all_dataframes],
                                 'raw': [df for df in all_dataframes]
                                 })
        # TODO this looks clunky
        return ivt_data


class Lakeshore(SerialInstrument):
    """
    Object to control the Lakeshore temperature sensor and controller
    """
    def __init__(self, com_port='COM3', serial_config=None, **kwargs):
        if serial_config is None:
            serial_config = {
                # 'port' : com_port,
                'timeout': 10, 'baudrate': 9600,
                'parity': serial.PARITY_ODD,
                'bytesize': serial.SEVENBITS,
                # 'eol': eol
            }
        super().__init__(com_port, serial_config=serial_config, **kwargs)
        self.loop = 0
        self.commanDic = {'QCLS': 'Clear Interface Command',
                          'QESE': 'Event Status Enabled',
                          'QESE?': 'Event Status Enabled Query',
                          'QESR?': 'Event Status Register Query',
                          'QIDN?': 'Identification Query',
                          'QOPC': 'Operation Complete Command',
                          'QOPC?': 'Operation Complete Query',
                          'QRST': 'Reset Instrument Command',
                          'QSRE': 'Service Request Enable command',
                          'QSRE?': 'Service Request Enable Query',
                          'QSTB?': 'Status Byte Query',
                          'QTST?': 'Self-Test Query',
                          'QWAI': 'Wait-To-Continue Command',
                          'CMODE': 'Control Loop Mode Command',
                          'CMODE?': 'Control Lop Mode Query',
                          'CRDG?': 'Celcius Reading',
                          'CRVDEL': 'Delete User Curve Command',
                          'CRVHDR': 'Curve Header Command',
                          'CRVHDR?': 'Curve Header Query',
                          'CRVPT': 'Curve Data Point Command',
                          'CRVPT?': 'Curve Data Point Query',
                          'CSET': 'Control Loop Parameter Command',
                          'CSET?': 'Control Loop Parameter Query',
                          'DFLT': 'Factory Defaults Command',
                          'DISPFLD': 'Display Field Command',
                          'DISPFLD?': 'Display Field Query',
                          'FILTER': 'Input Filter Parameter Command',
                          'FILTER?': 'Input Filter Parameter Query',
                          'HTR?': 'Heater Output Query',
                          'HTRRES': 'Heater Resistance Setting Command',
                          'HTRRES?': 'Heater Resistance Setting Query',
                          'IEEE': 'IEEE Interface Parameter Command',
                          'IEEE?': 'IEEE Interface Parameter Query',
                          'INCRV': 'Input Curve Number Command',
                          'INCRV?': 'Input Curve Number Query',
                          'INTYPE': 'Input Type Parameter Command',
                          'INTYPE?': 'Input Type Parameter Query',
                          'KEYST?': 'Keypad Status Query',
                          'KRDG?': 'Kelvin Reading Query',
                          'LOCK': 'Front Panel Keyboard Lock Command',
                          'LOCK?': 'Front Panel Keyboard Lock Query',
                          'MODE': 'Set Local/Remote Mode',
                          'MODE?': 'Query Local/Remote Mode',
                          'PID': 'Control Loop PID Values Command',
                          'PID?': 'Control Loop PID Values Query',
                          'RAMP': 'Control Loop Ramp Command',
                          'RAMP?': 'Control Loop Ramp Query',
                          'RAMPST?': 'Control Loop Ramp Status Query',
                          'REV?': 'Input Firmware Revision Query',
                          'SCAL': 'Generate SoftCal Curve Command',
                          'SETP': 'Control Loop Set Point Command',
                          'SETP?': 'Control Loop Set Point Query',
                          'SRDG?': 'Sensor Unit Reading Query',
                          'TEMP?': 'Room-Temperature Comp. Temp. Query',
                          'TLIMIT': 'Temperature Limit Command',
                          'TLIMIT?': 'Temperature Limit Query',
                          'TUNEST?': 'Control Loop 1 Tuning Query',
                          'Zone': 'Control Loop ZOne Table Command',
                          'Zone?': 'Control Loop ZOne Table Query'
                          }

    @property
    def temp(self):
        """
        Get the temperature from the lakeshore in degrees C
        :return: Temperature in C
        """
        new_t = float(self.ask("KRDG?"))
        time.sleep(0.2)
        return new_t

    @temp.setter
    def temp(self, new_temp):
        self.write(f'SETP {self.loop}, {new_temp:.3f}')

    @property
    def set_point(self):
        """
        Gets the setpoint for loop 0
        :return:
        """
        return self.ask(f'SETP? {self.loop}')

    def get_setpoint_by_loop(self, loop):
        return self.ask(f'SETP? {loop!s}')

    def set_temp_by_loop(self, loop, temp):
        self.loop = loop
        self.temp = temp

    def setTempAndWait(self, loop, temp, percent=.01):
        self.loop = loop
        self.temp = temp
        # self.write('SETP %s, %0.3f' % (loop, temp))
        i = 0
        while True:
            try:
                ct = np.array([float(self.temp) for i in range(10)])
                meanT = np.mean(ct)
                stdT = np.std(ct)
                if abs(meanT - temp) < percent * temp and (stdT < percent * temp):
                    return 'stable: %s' % meanT
                time.sleep(.2)
            except:
                i += 1
                if i > 5:
                    return 'error: could not read temp'

    def is_stable(self, tolerance=0.01, repeats=3):
        """
        Returns true if N consecutive temperature readings are within tolerance of the first one
        :param tolerance:
        :param repeats:
        :return:
        """
        temp = self.temp
        stable = [abs(self.temp - temp) < tolerance for _ in range(repeats)]
        if all(stable):
            return True
        else:
            return False
        #
        # i = 0
        # for _ in range(repeats):
        #     ct = self.temp
        #     if abs(ct - temp) < tolerance:
        #         i += 1
        # time.sleep(.2)
        # if i == 3:
        #     return True
        # else:
        #     return False


class CI_SR800R:
    """
    Extended area black body
    """
    def __init__(self, comPort, **kwargs):
        self.kwargs = {'timeout': 1}
        self.kwargs.update(kwargs)
        self.con = SerialInstrument(comPort, **self.kwargs)

    def getTemp(self):
        return self.con.ask('GETTEMPERATURE')

    def getTempSetPoint(self):
        return self.con.ask('GETABSOLUTESETPOINT')

    def setTemp(self, temp):
        self.con.write('SETTEMPERATURE %0.3f' % temp)

    def setTempAndWait(self, temp):
        self.con.write('SETTEMPERATURE %0.3f' % temp)
        while 1:
            r = self.con.query('ISTEMPERATURESTABLE')
            if r == '1':
                t = self.getTemp()
                return 'stable: %s' % t

    def isStable(self):
        return self.con.query('ISTEMPERATURESTABLE')


class CI_SR200(SerialInstrument):
    """
    Cavity black body
    """
    def __init__(self, com_port='COM5', **kwargs):
        super().__init__(com_port, **kwargs)
        self._aperture_i = 1
        self._filter_i = 1
        self.status = 'ready'
        self.commandList = ['GETAXISSTATUS',
                            'AXISHOMING',
                            'SETTARGET',
                            'SA',
                            'SETACTIVEAXIS',
                            'GETACTIVEAXIS',
                            'GETNUMBEROFAXES',
                            'RA',
                            'GETTARGETFREQUENCY',
                            'GETAXISEFL',
                            'GETTEMPERATURE',
                            'GETTEMPERATURERANGE',
                            'RG',
                            'ISTEMPERATURESTABLE',
                            'RE',
                            'SETTEMPERATURE',
                            'ST',
                            'GETINPUTSTATUS',
                            'OUTPUTENABLE',
                            'SETOUTPUT',
                            'ECHO',
                            'SETPRESET',
                            'GETMAXPRESETCOUNT',
                            '*IDN?',
                            'RV',
                            'GETBITERROR',
                            'GETSYSTEMINFO',
                            'CALIBRATETOUCHPANEL',
                            'SETREMOTETYPE',
                            'GETABSOLUTESETPOINT',
                            'SETCOMUPTERNAME',
                            'DM']

    @property
    def temp(self):
        """
        Temperature of the blackbody
        :return:
        """
        return float(self.ask('GETTEMPERATURE'))

    @temp.setter
    def temp(self, new_temp):
        self.write(f'SETTEMPERATURE {new_temp:d}')

    @property
    def set_point(self):
        return float(self.ask('GETABSOLUTESETPOINT'))

    @set_point.setter
    def set_point(self, val):
        raise NotImplementedError

    @property
    def aperture_list(self):
        self.ask('SETACTIVEAXIS 1')
        return self.ask('GETTARGETLIST')

    @property
    def filter_list(self):
        self.ask('SETACTIVEAXIS 2')
        return self.ask('GETTARGETLIST')

    @property
    def aperture(self):
        return self._aperture_i

    @aperture.setter
    def aperture(self, new_aperture):
        """
        Sets the aperture of the blackbody

        :param new_aperture: Integer corresponding to the proper aperture
        :return:
        """
        #TODO type check the input
        #todo fix this it can be better
        logger.debug(f"Moving aperture to {new_aperture}")
        if type(new_aperture) is str:
            apertures = self.aperture_list
            holder = ""
            #count = 0
            final_holder = ""
            apertures_final = []
            apertures_length = len(apertures)
            place = 0
            flagger = False
            apertures = str(apertures)
            while place <= apertures_length:
                try:
                    i = apertures[place]
                except:
                    flagger = True
                holder = i
                if i == "," or flagger == True:
             #       count = count + 1
                    apertures_final.append(final_holder)
                    final_holder = ""
                else:
                    final_holder = final_holder + holder
                place = place + 1
            del apertures_final[0]
            new_aperture = apertures_final.index(new_aperture)
        self._aperture_i = new_aperture
        self.ask('SETACTIVEAXIS 1')
        self.ask(f'SETTARGET {new_aperture:d}')
        while self.is_axis_moving():
            self.status = 'Aperture moving'
        self.status = 'ready'
        self.ask('RA')

    @property
    def filter(self):
        return self._filter_i

    @filter.setter
    def filter(self, new_filter):
        logger.debug(f"Moving filter to {new_filter}")
        self.ask('SETACTIVEAXIS 2')
        self.ask(f'SETTARGET {new_filter:d}')
        while self.is_axis_moving():
            self.status = 'Filter wheel moving'
        self.status = 'ready'
        self.ask('RA')

    def is_axis_moving(self):
        return self.ask('GETAXISSTATUS') != '0\r\n'

    def setTempAndWait(self, temp):
        self.temp = temp
        while True:
            self.status = 'Awaiting temperature stability'
            if self.is_stable():
                self.status = 'ready'
                return f'stable: {self.temp!s}'
            time.sleep(0.1)

    def is_stable(self):
        return self.ask('ISTEMPERATURESTABLE') == '1'

    def setAperture(self, n):
        self.aperture = n
        # print('moving to aperture: %s' % n)
        # self.con.ask('SETACTIVEAXIS 1')
        # self.con.ask('SETTARGET %d' % n)
        # while self.con.ask('GETAXISSTATUS') != '0':
        #     print('moving...')
        # return self.con.ask('RA')

    def setFilter(self, n):
        self.filter = n
        # print('moving to filter: %s' % n)
        # self.con.ask('SETACTIVEAXIS 2')
        # self.con.ask('SETTARGET %d' % n)
        # while self.con.ask('GETAXISSTATUS') != '0':
        #     print('moving...')
        # return self.con.ask('RA')