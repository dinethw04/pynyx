"""
Here are my software requirements for the python-enabled Nyx:

•	Must be able to acquire measurements from the Keithley 2400 including:
    o	A sweep of voltages and measured currents at each voltage
    o	A sweep of currents and measured voltages at each current
    o	A list of currents measured from a single voltage over a given time with no greater than 10ms between currents
    o	A list of voltages measured from a single sourced current over a given time with no delay greater than 10ms between voltages
    o	Ability to configure timing parameters from the keithley (source delay, nPLC, etc.)
    o	Ability to read the current configuration state of the keithley and store for later reference
    o	Capability for a soft and hard reset to default settings
    o	Direct control of the output being on or off
    o	Toggle for the front and rear ports
    o	Ability to send arbitrary string commands to the Keithley and return any parsed error code
    •	Must be able to set and measure temperature set points from the Lakeshore or other temp sensors
    o	Read current temperature in K or C
    o	Set the current setpoint in K or C
    o	Read the current sensor voltage or current directly
    o	Extract / load calibration curves for temperature sensors
    o	Ability to index different temperature sensors and read simultaneously (within 10ms)
    o	Ability to set PID values for temperature control
    o	Ability to enable or disable the heating element directly
    o	Ability to send arbitrary string commands to the instrument and return parsed error codes
    o	Soft and hard reset to default configuration
    o	Ability to read the current state of the instrument and its current configuration
•	Must be able to control the blackbody light source
    o	Set the current blackbody temperature in C or K
    o	Read the current blackbody temperature in C or K
    o	Adjust PID control or temperature stability
    o	Request a ‘status’ of the blackbody indicating if it has reached target temperature and a prediction of the remaining delay
    o	Ability to send arbitrary string commands to the instrument and return parsed error codes
    o	Soft and hard reset to default configuration
    o	Ability to read the current state of the instrument and its current configuration
•	Must be able to control spectral gratings and/or spectral filter wheels
    o	Set the target wavelength or spectral filter
    o	Read the current set wavelength or filter
    o	Read the current grating position or wheel position index
    o	Return a list of available wavelengths or filters
    o	Read state of the spectral system and its configuration
•	Software must be able to recover from a complete power outage during measurement.
    o	Once power is restored and software is started again, measurements must be able to be resumed from last saved state.
    o	A loss of 10 minutes of measurement time is acceptable
•	Code must make use of the ‘logging’ module and use proper log calls to track equipment status and use during operation.
•	Code must use source code control.
    o	Work is to be done on branches.
    o	Master branch will be pulled and used on live equipment.
    o	Pushes may not be done to Master until approved and suitable for lab use.
•	Data must be stored from a measurement in a robust way suitable for database uploading to SQL
    o	Data deletion is to be prevented
•	Data must be manually approved or ‘published’ before it is sent to SQL
    o	Approval process must compute certain metrics automatically (SNR, J0, D*, QE, etc.)
•	Software package must accept as input a formatted XML or JSON string, or accept a python script using a related module to control measurement functionality.
    o	In theory, a list of JSON’s which specify function calls or equipment calls could be the only needed input to run an entire experiment.
    o	If you allow module control, use of Assert calls, Logging module calls, and Try/Catch is mandatory
•	Software must be able to run ‘headless’ without a GUI
•	Software must support GUI calls

For a minimum viable product:
•	Basic voltage sweep, sense current from the Keithley is available from -2V to 2V with 1mA current limit, 10ms delay, NPLC=1
•	Blackbody temperature set and read
•	Temperature set and read
•	Data stored to tab-separated text file with headers
•	Ability to safe abort
•	Ability to hard reset equipment to a known configuration
•	Source code control basics

"""