#this is just imports
#!/usr/bin/env python3
import sys
from PyQt4 import QtGui
from PyQt4.QtCore import QThread, SIGNAL
import main_nyxe_three
import os
import numpy as np
import time, datetime
from pynyx.equipment import Keithley2400, CI_SR200, Lakeshore
from pynyx.measure import Radiometric
from pynyx import setup_logger, data
import pynyx
logger = setup_logger(__name__)
#This class handles the threading that will be needed for running calculations/results pages
#will prob need to add a matrix/array to __init__ to pass the variables that will be needed
#to start the calculations
#a thread can be called by doing 
#self.newThreadInstance = runningCalc()
#self.newThreadInstance.start()
#you should not have a start function instide the class it should do whatever is inside run according
#to docs
class calc_variables_thread(QThread):
	def __init__(self,name_type,start_val,stop,third_value,option_flag, parent = None):
		#start up of thread
		#super is inheriting everything from QThread and making sure it can do 
		#everything that QThread can do
		super(calc_variables_thread, self).__init__(parent)
		#defining variables to be used later
		self.name_type = name_type
		self.start_val = start_val 
		self.stop = stop
		self.third_value = third_value
		self.option_flag = option_flag


#dont know why this is here
#think it has to do with when you are terminating the thread
# u dont want it to be doing something so this tells the thread to wait then it gets killed
	def __del__self(self):
		self.wait()

#This is were the calculation for the text boxes are done for all pages
#by doing it here it stops the gui from freezing
#todo GUI will still freeze on large numbers / sometimes is unresponsive
#look into that
#TODO k you have to make new function for the aperture list and filter list
#it should pull the current lists from the machine using buurmas code  and then place them into
# a list and dictionary
	def _calculate(self, start_val, stop, third_value, option_flag,name_type):
		#print "now in calculate thread"
		start = float(str(start_val))
		stop = float(str(stop))
		third_value = float(str(third_value))
		data_values = []
		self.name_type=name_type
		print(name_type)
		print (start)
		print(stop)
		#self.emit(SIGNAL('send_text_field_name('))

		#Shouldnt be here but leaving it just in case something breaks
		#it was an indicator for when the first instance of data was transmitted but that
		#should be done in the main GUI class
		#self.calculation_switch = True

		##if this option is selected do a rudementary start stop step calc
		#and grab all of the values/send them back to the main thread through a signal
		if option_flag == "Start,Stop,Step":

			data_values.append(start)
			while (start <= stop):
				temp = start + third_value
				if (temp > stop):
					break
				else: 
					#print temp 
					data_values.append(temp)
					#self.emit(SIGNAL('add_value_text(PyQt_PyObject)'),temp)
					start = start + third_value
					#a flag to make sure no data is lost
					transfer_flag = False
					#if more than 300 values have been calculated send it back to main
					#thread
					if len(data_values) > 300:
						print("name_type")
						print(self.name_type)
						if self.name_type == 1:
							self.emit(SIGNAL('add_value_text(PyQt_PyObject)'),str(data_values))
							print ("in one")
						if self.name_type == 2:
							print ("in two")
							self.emit(SIGNAL('add_value_text_dewar(PyQt_PyObject)'),str(data_values))
						if self.name_type ==3:
							print ("in three")
							self.emit(SIGNAL('add_value_text_ivt(PyQt_PyObject)'),str(data_values))
						data_values = []
						transfer_flag = True 
						#print "transfered"

			#if last bit of data has not been transmitted then transfer it 
			print("before final trans")
			if transfer_flag == False:
				print("name_type")
				print(self.name_type)
				if self.name_type == 1:
					print("in one final")
					self.emit(SIGNAL('add_value_text(PyQt_PyObject)'),str(data_values))
				if self.name_type == 2:
					print ("in two final")
					self.emit(SIGNAL('add_value_text_dewar(PyQt_PyObject)'),str(data_values))
				if self.name_type ==3:
					print ("in three final")
					self.emit(SIGNAL('add_value_text_ivt(PyQt_PyObject)'),str(data_values))
				data_values = []
				#print "end transfer one"

		#same as above but with a diff option 
		#this time its start stop N
		if option_flag == "Start,Stop,N":
			data_values.append(start)
			n = (stop/third_value)
			print (n) 
			while (start <= stop):
				temp = start + n 
				if (temp > stop):
					break
				else:
					#print temp 
					transfer_flag = False
					self.emit(SIGNAL('add_value_text(QString)'),temp)
					data_values.append(temp)
					start = start + n
					if len(data_values) > 200:
						self.emit(SIGNAL('add_value_text(PyQt_PyObject)'),str(data_values))
						data_values = []
						transfer_flag = True 
						#print "transfered"
			if transfer_flag == False:
				self.emit(SIGNAL('add_value_text(PyQt_PyObject)'),str(data_values))
				#print "end transfer two"


		data_values = []


		#for some reason you still have this returning a string of numbers
		#no need for this 
		return str(data_values)
	#When a new instance of the thread is told to start
	#this function is called
	#it calls calculate
	def run(self):
		#print "here bitches"
		values = self._calculate(self.start_val,self.stop,self.third_value,self.option_flag,self.name_type)
		#self.emit(SIGNAL('add_value_text(str)'),values)


#this is the main work flow for the app
#the main menu will be populated in __init__ and set up what the buttons will do
#new pages will be defined and constructed in their own functions with a way to relate
#back to the main window

#TODO add a page for bandpass filters
class PyNyxe(QtGui.QMainWindow, main_nyxe_three.Ui_MainWindow):
	def __init__(self, parent = None):
		#setup stuff
		super(PyNyxe, self).__init__(parent)
		#called setupUi function from main_nyxe_tw.ui import which is a python file
		#this populates and makes the GUI
		#all of the interactions/actions happen afterwards
		self.setupUi(self)
		#making sure the main page is displayed and not something else
		#since we are using stackedwidgets the first page is 0
		#thats only because of the way i made the layout
		self.stackedWidget.setCurrentIndex(0)

		#all buttons for measurement options
		#will take the window to respective pages
		self.bb_temp_button.clicked.connect(lambda: self.stackedWidget.setCurrentIndex(1))
		self.dewar_temp_button.clicked.connect(lambda: self.stackedWidget.setCurrentIndex(2))
		self.bb_filter_button.clicked.connect(lambda: self.stackedWidget.setCurrentIndex(3))
		self.bb_aperture_button.clicked.connect(lambda: self.update_bb_aperture())
		self.ivt_button.clicked.connect(lambda: self.stackedWidget.setCurrentIndex(5))

		#all the back buttons from every page
		#will take the window back to start menu
		self.back_button.clicked.connect(lambda: self.stackedWidget.setCurrentIndex(0))
		self.back_button_dewar.clicked.connect(lambda: self.stackedWidget.setCurrentIndex(0))
		self.cancel_button_bb_filter.clicked.connect(lambda: self.stackedWidget.setCurrentIndex(0))
		self.cancel_button_bb_aperture.clicked.connect(lambda: self.stackedWidget.setCurrentIndex(0))
		self.back_ivt_button.clicked.connect(lambda: self.stackedWidget.setCurrentIndex(0))
		self.back_results.clicked.connect(lambda:self.stackedWidget.setCurrentIndex(0))
		self.Find_sample_button.clicked.connect(lambda: self.grab_sample_names())

		#config button to load config file
		self.load_button.clicked.connect(lambda: self.load_config())
		#save button to save measurement loops/parameters
		self.save_button.clicked.connect(lambda: self.save_config())

		#setting up the dropdown menu for the pages that have them
		self.comboBox_bb_temp.currentIndexChanged.connect(lambda: self.change_options_bb_temp())
		self.combo_dewar.currentIndexChanged.connect(lambda: self.change_options_dewar_temp())
		self.comboBox_ivt.currentIndexChanged.connect(lambda: self.change_options_ivt())

		self.calculate_button.clicked.connect(lambda: self.calc_bb_temp())
		self.calculate_button_dewar.clicked.connect(lambda: self.calc_dewar_temp())
		self.calculate_ivt_button.clicked.connect(lambda: self.calc_ivt())

		self.pushButton_2.clicked.connect(lambda: self.clear_bb_temp())
		self.pushButton_3.clicked.connect(lambda: self.clear_dewar_temp())
		self.pushButton_5.clicked.connect(lambda: self.clear_ivt())

		self.accept_button.clicked.connect(lambda: self.accept_bb_temp())
		self.accept_button_dewar.clicked.connect(lambda: self.accept_dewar_temp())
		self.accept_ivt_button.clicked.connect(lambda: self.accept_ivt())

		self.button_one_bb_filter.clicked.connect(lambda: self.button_one_bb_filter_action())
		self.button_two_bb_filter.clicked.connect(lambda: self.button_two_bb_filter_action())
		self.button_three_bb_filter.clicked.connect(lambda: self.button_three_bb_filter_action())
		self.button_four_bb_filter.clicked.connect(lambda: self.button_four_bb_filter_action())
		self.button_five_bb_filter.clicked.connect(lambda: self.button_five_bb_filter_action())
		self.button_six_bb_filter.clicked.connect(lambda: self.button_six_bb_filter_action())
		self.button_seven_bb_filter.clicked.connect(lambda: self.button_seven_bb_filter_action())
		self.button_eight_bb_filter.clicked.connect(lambda: self.button_eight_bb_filter_action())



		self.accept_button_bb_filter.clicked.connect(lambda: self.bb_filter_accept())


		self.button_one_bb_aperture.clicked.connect(lambda: self.button_one_bb_aperture_action())
		self.button_two_bb_aperture.clicked.connect(lambda: self.button_two_bb_aperture_action())
		self.button_three_bb_aperture.clicked.connect(lambda: self.button_three_bb_aperture_action())
		self.button_four_bb_aperture.clicked.connect(lambda: self.button_four_bb_aperture_action())
		self.button_five_bb_aperture.clicked.connect(lambda: self.button_five_bb_aperture_action())
		self.button_six_bb_aperture.clicked.connect(lambda: self.button_six_bb_aperture_action())
		self.button_seven_bb_aperture.clicked.connect(lambda: self.button_seven_bb_aperture_action())
		self.button_eight_bb_aperture.clicked.connect(lambda: self.button_eight_bb_aperture_action())


		self.append_aperture.clicked.connect(lambda: self.add_bpf_to_apeture())

		self.pushButton_4.clicked.connect(lambda: self.bb_aperture_accept())

		



		#dont know why this is here 
		#must have been used by something have to trace
		option_flag = self.comboBox_bb_temp.currentText()
		#the start measurement button
		#calls function that will grab all data and pass to measurement api class
		self.start_measurement_button.clicked.connect(lambda: self.start_measurement())

#todo fix this it no longer works, all threading is broken for some reason
		#self.running_calc = calc_variables_thread(option_flag, self.start_field.text(), self.stop_field.text(),self.step_field.text(), option_flag)

		#self.connect(self.running_calc, SIGNAL("add_value_text(QString)"),self.add_value_text)
		#print type(self.running_calc)


		#grabs and populates pages with filter and aperture list options
		
		
		try:
			self.get_list_data()
		except:
			print("nope")

		self.apertures_dict = {}

	def update_bb_aperture(self):
		for i in self.bb_filter_values_list:
			self.comboBox_aperture.addItem(str(i))
		self.stackedWidget.setCurrentIndex(4)

#is now switched to add aperture to bpf because i had them reversed
	def add_bpf_to_apeture(self):
		values = str(self.bb_aperture_text_edit.toPlainText())
		values = values.split(",")
		print('make sure t is still here')
		print(values)
		values = values[:-1]

		self.apertures_dict[self.comboBox_aperture.currentText()] = values
		self.bb_aperture_text_edit.clear()
		print (self.apertures_dict[self.comboBox_aperture.currentText()])


		


	def button_one_bb_aperture_action(self):
		self.bb_aperture_text_edit.insertPlainText(self.button_one_bb_aperture.text())
		self.bb_aperture_text_edit.insertPlainText(",")
	def button_two_bb_aperture_action(self):
		self.bb_aperture_text_edit.insertPlainText(self.button_two_bb_aperture.text())
		self.bb_aperture_text_edit.insertPlainText(",")
	def button_three_bb_aperture_action(self):
		self.bb_aperture_text_edit.insertPlainText(self.button_three_bb_aperture.text())
		self.bb_aperture_text_edit.insertPlainText(",")
	def button_four_bb_aperture_action(self):
		self.bb_aperture_text_edit.insertPlainText(self.button_four_bb_aperture.text())
		self.bb_aperture_text_edit.insertPlainText(",")
	def button_five_bb_aperture_action(self):
		self.bb_aperture_text_edit.insertPlainText(self.button_five_bb_aperture.text())
		self.bb_aperture_text_edit.insertPlainText(",")
	def button_six_bb_aperture_action(self):
		self.bb_aperture_text_edit.insertPlainText(self.button_six_bb_aperture.text())
		self.bb_aperture_text_edit.insertPlainText(",")
	def button_seven_bb_aperture_action(self):
		self.bb_aperture_text_edit.insertPlainText(self.button_seven_bb_aperture.text())
		self.bb_aperture_text_edit.insertPlainText(",")
	def button_eight_bb_aperture_action(self):
		self.bb_aperture_text_edit.insertPlainText(self.button_eight_bb_aperture.text())
		self.bb_aperture_text_edit.insertPlainText(",")


	def button_one_bb_filter_action(self):

		self.blackbody_filter_text_edit.insertPlainText(self.button_one_bb_filter.text())
		self.blackbody_filter_text_edit.insertPlainText(",")
	def button_two_bb_filter_action(self):

		self.blackbody_filter_text_edit.insertPlainText(self.button_two_bb_filter.text())
		self.blackbody_filter_text_edit.insertPlainText(",")
	def button_three_bb_filter_action(self):

		self.blackbody_filter_text_edit.insertPlainText(self.button_three_bb_filter.text())
		self.blackbody_filter_text_edit.insertPlainText(",")
	def button_four_bb_filter_action(self):

		self.blackbody_filter_text_edit.insertPlainText(self.button_four_bb_filter.text())
		self.blackbody_filter_text_edit.insertPlainText(",")

	def button_five_bb_filter_action(self):

		self.blackbody_filter_text_edit.insertPlainText(self.button_five_bb_filter.text())
		self.blackbody_filter_text_edit.insertPlainText(",")
	def button_six_bb_filter_action(self):

		self.blackbody_filter_text_edit.insertPlainText(self.button_six_bb_filter.text())
		self.blackbody_filter_text_edit.insertPlainText(",")
	def button_seven_bb_filter_action(self):

		self.blackbody_filter_text_edit.insertPlainText(self.button_seven_bb_filter.text())
		self.blackbody_filter_text_edit.insertPlainText(",")
	def button_eight_bb_filter_action(self):

		self.blackbody_filter_text_edit.insertPlainText(self.button_eight_bb_filter.text())
		self.blackbody_filter_text_edit.insertPlainText(",")

	#calls up a explorer to find the .txt config file
	def get_list_data(self):
		#TODO: handle getting comport and checking make sure
		#TTODO: that i am giving the right one
		self.blackbody = CI_SR200()

		apertures = self.blackbody.aperture_list
		filters = self.blackbody.filter_list
		print(apertures)
		print(filters)

		holder = ""
		count = 0
		final_holder = ""
		apertures_final = [None] * 200
		apertures_length = len(apertures)
		place = 0
		flagger = False
		apertures = str(apertures)
		print (apertures_length)
		while place <= apertures_length:
			try:
				i = apertures[place]
			except:
				flagger = True
			holder = i
			if i == "," or flagger == True:
				count = count + 1
				apertures_final[count] = final_holder
				final_holder = ""
				print("got to thing")
			else:
				final_holder = final_holder + holder
			place = place + 1
		print("got to end of apertures calc")
		del apertures_final[0]

		for i in apertures_final:
			print(i)



		holder = ""
		count = 0
		final_holder = ""
		filters_final = [None] * 50
		filters_length = len(filters)
		place = 0
		flagger = False
		filters = str(filters)
		print (filters_length)
		while place <= filters_length:
			try:
				i = filters[place]
			except:
				flagger = True
			holder = i
			if i == "," or flagger == True:
				count = count + 1
				filters_final[count] = final_holder
				final_holder = ""
			else:
				final_holder = final_holder + holder
			place = place + 1

		apertures_final_two = [None] * 16
		filters_final_two = [None] * 16
		count = 0





		for i in apertures_final:
			try:
				x = len(str(i))
				if x <= 3:
					pass
				if i == None:
					pass
				else:
					apertures_final_two[count] = i
					print (i)
					count = count + 1
				
			except:
				print("was null")

		print("got to end")

		count = 0

		for i in filters_final:
			try:
				print('stuff for bbfilter')
				print(count)
				print(i)
				count = count + 1
			except:
				print("was null")

		print("got to end")

		self.button_one_bb_aperture.setText(str(apertures_final_two[0]))
		self.button_two_bb_aperture.setText(str(apertures_final_two[2]))
		self.button_three_bb_aperture.setText(str(apertures_final_two[4]))
		self.button_four_bb_aperture.setText(str(apertures_final_two[6]))
		self.button_five_bb_aperture.setText(str(apertures_final_two[8]))
		self.button_six_bb_aperture.setText(str(apertures_final_two[10]))
		self.button_seven_bb_aperture.setText(str(apertures_final_two[12]))
		self.button_eight_bb_aperture.setText(str(apertures_final_two[14]))

		self.button_one_bb_filter.setText(str(filters_final[1]))
		self.button_two_bb_filter.setText(str(filters_final[3]))
		self.button_three_bb_filter.setText(str(filters_final[5]))
		self.button_four_bb_filter.setText(str(filters_final[7]))
		self.button_five_bb_filter.setText(str(filters_final[9]))
		self.button_six_bb_filter.setText(str(filters_final[11]))
		self.button_seven_bb_filter.setText(str(filters_final[13]))
		self.button_eight_bb_filter.setText(str(filters_final[15]))



	def load_config(self):
		#creates an instance of the file explorer
		dialog = QtGui.QFileDialog()
		#telling it to only show folders and text files
		#it returns a list
		self.config_folder_path = dialog.getOpenFileNames(self, "Select File", "", "*.txt")
		#getting the folder path and opeinig it so that you can sort through the data
		file_object = open(str(self.config_folder_path[0]),"r")
		loops = file_object.readlines()
		file_object.close()
		beggin = False
		data_loops = [None]*5
		holder = []

		#goin through each thing within the file and grabbing the data
		value = 0
		for row in loops:
			if row[0] == "[":
				beggin = True
				holder.append(row)
				check = len(row)
				if row[(check-1)] == "]" or row[(check-1)] == "\n":
					beggin = False
					data_loops[value] = holder
					holder = []
					value = value +1

			if beggin == True:
				if row[-1] == "]":
					holder.append(row)
					data_loops[value] = holder
					value = value + 1
				else:
					holder.append(row)

		#this is formating the strings to be placed in labels and text boxes

		holder = ".....,....."
		values = data_loops[0]
		values = values[0][0:-1]

		values = str(values)
		values = values.split(",")
		#print "values"
		#print values

		first = values[0:7]
		#print "first"
		#print first
		last = values[-7:]

		bb_label = str(first) + str(holder) + str(last)
		#print "bb_label"
		#print bb_label


		self.bb_temp_label.clear()
		self.bb_temp_label.setText(str(bb_label))

		self.bb_temp_text_edit.clear()



		values = str(values)
		values = values.replace("'","").replace(' ','')
		values = values[1:-1]
		print (values)

		self.bb_temp_text_edit.insertPlainText(values)






		holder = ".....,....."
		values = data_loops[1]
		values = values[0][0:-1]

		values = str(values)
		values = values.split(",")
		#print "values"
		#print values

		first = values[0:7]
		#print "first"
		#print first
		last = values[-7:]

		bb_label = str(first) + str(holder) + str(last)
		#print "bb_label"
		#print bb_label

		self.dewar_temp_label.clear()
		self.dewar_temp_label.setText(str(bb_label))
		self.stackedWidget.setCurrentIndex(0)

		self.dewar_temp_text_edit.clear()



		values = str(values)
		values = values.replace("'","").replace(' ','')
		values = values[1:-1]
		print (values)

		self.dewar_temp_text_edit.insertPlainText(values)






		holder = ".....,....."
		values = data_loops[2]
		values = values[0][0:-1]

		values = str(values)
		values = values.split(",")
		#print "values"
		#print values

		first = values[0:7]
		#print "first"
		#print first
		last = values[-7:]

		bb_label = str(first) + str(holder) + str(last)
		#print "bb_label"
		#print bb_label

		self.bb_filter_label.clear()
		self.bb_filter_label.setText(str(bb_label))
		self.stackedWidget.setCurrentIndex(0)

		self.blackbody_filter_text_edit.clear()



		values = str(values)
		values = values.replace("'","").replace(' ','')
		values = values[1:-1]
		print (values)

		self.blackbody_filter_text_edit.insertPlainText(values)



		holder = ".....,....."
		values = data_loops[3]
		values = values[0][0:-1]

		values = str(values)
		values = values.split(",")
		#print "values"
		#print values

		first = values[0:7]
		#print "first"
		#print first
		last = values[-7:]

		bb_label = str(first) + str(holder) + str(last)
		#print "bb_label"
		#print bb_label

		self.bb_aperture_label.clear()
		self.bb_aperture_label.setText(str(bb_label))
		self.stackedWidget.setCurrentIndex(0)

		self.bb_aperture_text_edit.clear()



		values = str(values)
		values = values.replace("'","").replace(' ','')
		values = values[1:-1]
		print (values)

		self.bb_aperture_text_edit.insertPlainText(values)



		holder = ".....,....."
		values = data_loops[4]
		values = values[0][0:-1]

		values = str(values)
		values = values.split(",")
		#print "values"
		#print values

		first = values[0:7]
		#print "first"
		#print first
		last = values[-7:]

		bb_label = str(first) + str(holder) + str(last)
		#print "bb_label"
		#print bb_label

		self.ivt_label.clear()
		self.ivt_label.setText(str(bb_label))
		self.stackedWidget.setCurrentIndex(0)

		self.ivt_text_edit.clear()



		values = str(values)
		values = values.replace("'","").replace(' ','')
		values = values[1:-1]
		print (values)

		self.ivt_text_edit.insertPlainText(values)


		#returns the file name
		#config_folder_path is a list
		#print self.config_folder_path[0]
		self.config_name = os.path.basename(unicode(self.config_folder_path[0]))
		#print self.config_name
		self.default_label.setText(self.config_name)
		
		#grabs whats inside of config_folder_path
		#i actually dont even need the for loop thats only if your opening multiple files
		#which is never the case in this program
		#for name in self.config_folder_path:
		#	text = open(name).read()
		#	print text


	#when save is pressed it will grab all values in the text edit boxes and then write
	#them to a file so that it can be loaded later
	def save_config(self):
		#grabing values from text edit boxes
		bb_temp_values = str(self.bb_temp_text_edit.toPlainText())
		dewar_temp_values = str(self.dewar_temp_text_edit.toPlainText())
		bb_filter_values = str(self.blackbody_filter_text_edit.toPlainText())
		bb_aperture_values = str(self.bb_aperture_text_edit.toPlainText())
		ivt_values = str(self.ivt_text_edit.toPlainText())
		print (bb_temp_values)
		print (dewar_temp_values)
		print (bb_filter_values)
		print (bb_aperture_values)
		print (ivt_values)

		#placeing the values along with their respective headers into the file
		save_name = QtGui.QFileDialog.getSaveFileName(self,'Save File',"", "*.txt")
		file = open(save_name,'w')
		file.write("BB_Temp:\n")
		file.write(bb_temp_values)
		file.write("\n")
		file.write("Det_Temp:\n")
		file.write(dewar_temp_values)
		file.write("\n")
		file.write("BB_Filt:\n")
		file.write(bb_filter_values)
		file.write("\n")
		file.write("BB_AP:\n")
		file.write(bb_aperture_values)
		file.write("\n")
		file.write("I(t)V:\n")
		file.write(ivt_values)
		file.write("\n")
		file.close()

	#used to get the ID names when the find button is pressed
	#it will put all names into a dict
	def grab_sample_names(self):
		sample_id = self.sample_id_field.text()
		dewar_id = self.dewar_id_field.text()
		device_id = self.device_id_field.text()

		sample_info= {'sample_id':sample_id, 'dewar_id': dewar_id,'device_id':device_id}

		for i in sample_info:
			print (i)
			print (sample_info[i])


	#the following functions just check what option that the user has selected in the dropdown menu
	#TODO add the functionality to switch the actually buttons/text fields when dropdown option is selected
	def change_options_bb_temp(self):
		user_option_bb_temp = self.comboBox_bb_temp.currentText()
		if user_option_bb_temp == "Start,Stop,N":
			self.step_label.setText("N")
		if user_option_bb_temp == "Start,Stop,Step":
			self.step_label.setText("Step")


		print (user_option_bb_temp)

	def change_options_dewar_temp(self):
		user_option_dewar_temp = self.combo_dewar.currentText()

		if user_option_dewar_temp == "Start,Stop,Step":
			self.step_label_dewar.setText("Step")
		if user_option_dewar_temp == "Start,Stop,N":
			self.step_label_dewar.setText("N")
		print (user_option_dewar_temp)

	def change_options_ivt(self):
		user_option_ivt = self.comboBox_ivt.currentText()

		if user_option_ivt == "Start,Stop,Step":
			self.step_ivt_label.setText("Step")
		if user_option_ivt == "Start,Stop,N":
			self.step_ivt_label.setText("N")
		print (user_option_ivt)

	def start_measurement(self):
		#used for sample name in radiometric
		self.sample_name = self.sample_id_field.text()
		self.dewar_id = self.dewar_id_field.text()
		self.device_id = self.device_id_field.text()
		self.sample_name_final={'sample_name': 'None','dewar_id': 'None', 'device_id': 'None', 'group_id':'Temp_nothing','name':'None'}
		self.sample_name_final['sample_name'] = str(self.sample_name)
		self.sample_name_final['dewar_id']= str(self.dewar_id)
		self.sample_name_final['device_id'] = str(self.device_id)
		self.sample_name_final['name'] = self.sample_name_final['sample_name'] + self.sample_name_final['dewar_id'] + self.sample_name_final['device_id']

		#used for bb_temps in radiometric

		#todo tomorrow do the thing you did when you hit accept everywhere else
		#parse this shit
		self.bb_temps_final = str(self.bb_temp_text_edit.toPlainText())
		#used for dewar_temps in radiometric
		self.dewar_temps_final = str(self.dewar_temp_text_edit.toPlainText())
		#used for bandpass_filters in radiometric
		self.bb_filter_final = str(self.blackbody_filter_text_edit.toPlainText())
		#used for apertures_dict in radiometric
		self.bb_aperture_final = str(self.bb_aperture_text_edit.toPlainText())

		#todo fix passing ivt arguments
		self.ivt_final = str(self.ivt_text_edit.toPlainText())


		self.bb_temps_final = self.bb_temps_final[1:-1]
		self.dewar_temps_final = self.dewar_temps_final[1:-1]
		self.bb_filter_final = self.bb_filter_final[1:-1]
		self.bb_aperture_final = self.bb_aperture_final[1:-1]

		self.bb_temps_final = self.bb_temps_final.split(",")
		self.dewar_temps_final = self.dewar_temps_final.split(",")
		self.bb_filter_final = self.bb_filter_final.split(",")
		self.bb_aperture_final = self.bb_aperture_final.split(",")


		

		print("self.sample_name")
		print(self.sample_name)
		print("self.dewar_id")
		print(self.dewar_id)
		print("self.device_id_field")
		print(self.device_id)
		print("self.sample_name whole")
		print(self.sample_name_final)
		print("self.bb_temps")
		print(self.bb_temps_final)
		print("self.dewar_temps")
		print(self.dewar_temps_final)
		print("self.bb_filter")
		print(self.bb_filter_final)
		print("self.bb_aperture")
		print(self.bb_aperture_final)
		print("self.ivt")
		print(self.ivt_final)

		dialog = QtGui.QFileDialog()
		#telling it to only show folders and text files
		#it returns a list
		self.out_path = dialog.getExistingDirectory(self, "Select Directory")
		self.out_path=str(self.out_path)
		smu = Keithley2400()
		temp_control = Lakeshore()
		if self.blackbody == None:
			print('blackbody was none something went wrong')
			print('attempting to remake')
			self.blackbody = CI_SR200()
		equipment = [smu, self.blackbody]
		self.sample = self.sample_name

		metadata = {'group_id' : data.get_group_id()}


		smu.config_sweep_iv(start=(int(self.start_ivt_field.text())), stop=(int(self.stop_ivt_field.text())),step=(int(self.step_field_ivt.text())))

		for temp_instance in self.bb_temps_final:
			self.blackbody.temp = int(float(temp_instance))
			for dewar_temp in self.dewar_temps_final:
				temp_control.temp = int(float(dewar_temp))
				for bpf in self.bb_filter_final:
					#todo since all filters are none will pass 1 just so the mvp will work
					#todo fix that
					self.blackbody.filter = 1
					bpf = 'F' + bpf
					for aperture in self.apertures_dict[bpf]:
						if aperture == "" or aperture == " " or aperture == None:
							pass
						else:
							self.blackbody.aperture = aperture
							while not self.blackbody.is_stable() and not temp_control.is_stable():
								print("Waiting for temps to stabilize")
								time.sleep(1)
							ivt_data_frame = smu.sweep_i_vs_t()
							metadata['date-time'] = str(datetime.datetime.now())
							metadata['Sweep_ID'] = 'S' + str(data.create_unique_id())
							metadata['BB_set_temp_(C)'] = str(float(temp_instance))
							print('what is being sent to bb_readthing')
							print(type(str(float(self.blackbody.temp))))
							print('original value')
							print(type(self.blackbody.temp))
							print(self.blackbody.temp)
							metadata['BB_read_temp_(C)'] = str(float(self.blackbody.temp))
							metadata['Dewar_set_temp_(C)'] = str(float(dewar_temp))
							metadata['Dewar_read_temp_(C)'] = str(float(temp_control.temp))
							data.save_ivt_files(sample=self.sample_name_final, ivt_data=ivt_data_frame, metadata=metadata, folder=self.out_path, single_file='zip')


	#redundate no longer needed
	#todo this is now handled in the calculation thread
	#check later then remove
	def calculate(self, start, stop, third_value, option_flag):
		start = float(str(start))
		stop = float(str(stop))
		third_value = float(str(third_value))
		data_values = []

		if option_flag == 1:
			data_values.append(start)
			while (start <= stop):
				temp = start + third_value
				if (temp > stop):
					break
				else: 
					data_values.append(temp)
					start = start + third_value

		if option_flag == 2:
			data_values.append(start)
			n = (stop/third_value)
			print (n) 
			while (start <= stop):
				temp = start + n 
				if (temp > stop):
					break
				else:
					print (temp) 
					data_values.append(temp)
					start = start + n

		return str(data_values)



	def calc_bb_temp(self):
		option_flag = self.comboBox_bb_temp.currentText()
		start_val_thread = self.start_field.text()
		stop_val_thread = self.stop_field.text()
		third_value_thread = self.step_field.text()

#		self.running_calc_bb = calc_variables_thread(1, start_val_thread,stop_val_thread,third_value_thread, option_flag)
#
#		self.connect(self.running_calc_bb, SIGNAL("add_value_text(PyQt_PyObject)"), self.add_value_text)
#		self.connect(self.running_calc_bb, SIGNAL("finished()"), self.done)

#		self.calculation_switch = True


#		self.running_calc_bb.start()

		#this is redudundant 
		#todo
		#check and make sure this is no longer needed
		#but this action should now be done by the calculation thread
		#was just here as a template just in case something went wrong
		if option_flag == "Start,Stop,Step":
			data_values = self.calculate(self.start_field.text(),self.stop_field.text(), 
				self.step_field.text(),1)

			#check the text box for what the previous data was
			old_string = self.bb_temp_text_edit.toPlainText()
			#take out the brackets and add a comma
			print (old_string)

			check = "[]"
			if old_string == check or old_string == "":
				self.bb_temp_text_edit.clear()
				old_string = ""
			else:
				old_string = old_string[:-1] + ","
				data_values = data_values[1:] 


			#place whatever in the text field
			data_values = old_string + data_values
			self.bb_temp_text_edit.clear()
			self.bb_temp_text_edit.insertPlainText(data_values)

		if option_flag == "Start,Stop,N":
			data_values = self.calculate(self.start_field.text(),self.stop_field.text(), 
				self.step_field.text(),2)

			#check the text box for what the previous data was
			old_string = self.bb_temp_text_edit.toPlainText()
			#take out the brackets and add a comma
			print (old_string)
			check = "[]"
			if old_string == check or old_string == "":
				self.bb_temp_text_edit.clear()
				old_string = ""
			else:
				old_string = old_string[:-1] + ","
				data_values = data_values[1:] 

			#place whatever in the text field
			data_values = old_string + data_values
			self.bb_temp_text_edit.clear()
			self.bb_temp_text_edit.insertPlainText(data_values)
			


	def calc_dewar_temp(self):

		option_flag = self.combo_dewar.currentText()


#		self.running_calc_dewar = calc_variables_thread(2, self.start_field_dewar.text(),self.stop_field_dewar.text(), 
#				self.step_field_dewar.text(), option_flag)

#		self.connect(self.running_calc_dewar, SIGNAL("add_value_text_dewar(PyQt_PyObject)"), self.add_value_text_dewar)
#		self.connect(self.running_calc_dewar, SIGNAL("finished()"), self.done)

#		self.calculation_switch_dewar = True


#		self.running_calc_dewar.start()
		#this is redudundant 
		#todo
		#check and make sure this is no longer needed
		#but this action should now be done by the calculation thread
		#was just here as a template just in case something went wrong


		if option_flag == "Start,Stop,Step":
			data_values = self.calculate(self.start_field_dewar.text(),self.stop_field_dewar.text(), 
				self.step_field_dewar.text(),1)

			old_string = self.dewar_temp_text_edit.toPlainText()
			#take out the brackets and add a comma
			check = "[]"
			if old_string == check or old_string == "":
				old_string = ""
				self.dewar_temp_text_edit.clear()
			else:
				old_string = old_string[:-1] + ","
				data_values = data_values[1:] 

			#place whatever in the text field
			data_values = old_string + data_values
			self.dewar_temp_text_edit.clear()
			self.dewar_temp_text_edit.insertPlainText(data_values)

		if option_flag == "Start,Stop,N":
			data_values = self.calculate(self.start_field_dewar.text(),self.stop_field_dewar.text(), 
				self.step_field_dewar.text(),2)

			#check the text box for what the previous data was
			old_string = self.dewar_temp_text_edit.toPlainText()
			#take out the brackets and add a comma
			check = "[]"
			if old_string == check or old_string == "":
				old_string = ""
				self.dewar_temp_text_edit.clear()
			else:
				old_string = old_string[:-1] + ","
				data_values = data_values[1:] 

			#place whatever in the text field
			data_values = old_string + data_values
			self.dewar_temp_text_edit.clear()
			self.dewar_temp_text_edit.insertPlainText(data_values)

	def calc_ivt(self):
		option_flag = self.comboBox_ivt.currentText()

#		self.running_calc_ivt = calc_variables_thread(3, self.start_ivt_field.text(),self.stop_ivt_field.text(), 
#				self.step_field_ivt.text(), option_flag)

#		self.connect(self.running_calc_ivt, SIGNAL("add_value_text_ivt(PyQt_PyObject)"), self.add_value_text_ivt)
#		self.connect(self.running_calc_ivt, SIGNAL("finished()"), self.done)

#		self.calculation_switch_ivt = True


#		self.running_calc_ivt.start()
		#this is redudundant 
		#todo
		#check and make sure this is no longer needed
		#but this action should now be done by the calculation thread
		#was just here as a template just in case something went wrong


		if option_flag == "Start,Stop,Step":
			data_values = self.calculate(self.start_ivt_field.text(),self.stop_ivt_field.text(), 
				self.step_field_ivt.text(),1)

			old_string = self.ivt_text_edit.toPlainText()
			#take out the brackets and add a comma
			check = "[]"
			if old_string == check or old_string == "":
				old_string = ""
				self.ivt_text_edit.clear()
			else:
				old_string = old_string[:-1] + ","
				data_values = data_values[1:] 

			#place whatever in the text field
			data_values = old_string + data_values
			self.ivt_text_edit.clear()
			self.ivt_text_edit.insertPlainText(data_values)

		if option_flag == "Start,Stop,N":
			print ("flagged")
			data_values = self.calculate(self.start_ivt_field.text(),self.stop_ivt_field.text(), 
				self.step_field_ivt.text(),2)

			#check the text box for what the previous data was
			old_string = self.ivt_text_edit.toPlainText()
			#take out the brackets and add a comma
			check = "[]"
			if old_string == check or old_string == "":
				old_string = ""
				self.ivt_text_edit.clear()
			else:
				old_string = old_string[:-1] + ","
				data_values = data_values[1:] 

			#place whatever in the text field
			data_values = old_string + data_values
			self.ivt_text_edit.clear()
			self.ivt_text_edit.insertPlainText(data_values)


#these function just clear the text edit field
	def clear_bb_temp(self):
		new_string = ""
		self.bb_temp_text_edit.clear()
		self.bb_temp_text_edit.insertPlainText(new_string)

	def clear_dewar_temp(self):
		new_string = ""
		self.dewar_temp_text_edit.clear()
		self.dewar_temp_text_edit.insertPlainText(new_string)

	def clear_ivt(self):
		new_string = ""
		self.ivt_text_edit.clear()
		self.ivt_text_edit.insertPlainText(new_string)

#when yiou accept the variables in the text edit field
#this will summarize the data 
#first and last 3 then put that summary into the corresponding label

	def bb_aperture_accept(self):
		values = str(self.bb_aperture_text_edit.toPlainText())
		holder = ["...","..."]
		values = values.split(",")
		first = values[0:2]
		last = values[-2:]
		bb_label = first + holder + last
		self.bb_aperture_label.clear()
		self.bb_aperture_label.setText(str(bb_label))
		self.stackedWidget.setCurrentIndex(0)


	def bb_filter_accept(self):
		values = str(self.blackbody_filter_text_edit.toPlainText())
		holder = ["...","..."]
		values = values.split(",")
		self.bb_filter_values_list = values
		first = values[0:2]
		last = values[-2:]
		bb_label = first + holder + last
		self.bb_filter_label.clear()
		self.bb_filter_label.setText(str(bb_label))
		self.stackedWidget.setCurrentIndex(0)

	def accept_bb_temp(self):
		values = str(self.bb_temp_text_edit.toPlainText())
		holder = ["...","..."]
		values = values.split(", ")
		first = values[0:3]
		last = values[-3:]
		bb_label = first + holder + last
		self.bb_temp_label.clear()
		self.bb_temp_label.setText(str(bb_label))
		self.stackedWidget.setCurrentIndex(0)


	def accept_dewar_temp(self):
		values = str(self.dewar_temp_text_edit.toPlainText())
		holder = ["...","..."]
		values = values.split(", ")
		first = values[0:3]
		last = values[-3:]
		bb_label = first + holder + last
		self.dewar_temp_label.clear()
		self.dewar_temp_label.setText(str(bb_label))
		self.stackedWidget.setCurrentIndex(0)


	def accept_ivt(self):
		values = str(self.ivt_text_edit.toPlainText())
		holder = ["...","..."]
		values = values.split(", ")
		first = values[0:3]
		last = values[-3:]
		bb_label = first + holder + last
		self.ivt_label.clear()
		self.ivt_label.setText(str(bb_label))
		self.stackedWidget.setCurrentIndex(0)

#is what the thread signal emits to 
#this function will take the data store it and populate it into the corresponding text 
#fields
	def add_value_text(self,instance_value_calc):

		#print "instance_value_calc"
		#print instance_value_calc

		if self.calculation_switch == True:
			#check the text box for what the previous data was
			old_string = self.bb_temp_text_edit.toPlainText()
			#take out the brackets and add a comma
			#print old_string
			check = "[]"
			if old_string == check or old_string == "":
				self.bb_temp_text_edit.clear()
				old_string = ""
			else:
				old_string = old_string[:-1] + ","
				#this is taking what the previous calc function did(returning list of shit)
				#taking out the beggining bracket [
				#data_values = data_values[1:] 
			#place whatever in the text field
			data_values = str(old_string) + str(instance_value_calc)
			self.calculation_switch = False
			#print "first data transmit"
		else:
			old_string = self.bb_temp_text_edit.toPlainText()
			#print "old_string[:-1]"
			#print old_string[:-1]
			old_string = old_string + ", "
			data_values = str(old_string) + str(instance_value_calc[1:])
			#print "data transmite"

		self.bb_temp_text_edit.clear()
		self.bb_temp_text_edit.insertPlainText(data_values)
		#print "made it past redraw"

#is what the thread signal emits to 
#this function will take the data store it and populate it into the corresponding text 
#fields
	def add_value_text_dewar(self,instance_value_calc):

		print ("instance_value_calc")

		if self.calculation_switch_dewar == True:
			#check the text box for what the previous data was
			old_string = self.bb_temp_text_edit.toPlainText()
			#take out the brackets and add a comma
			#print old_string
			check = "[]"
			if old_string == check or old_string == "":
				self.bb_temp_text_edit.clear()
				old_string = ""
			else:
				old_string = old_string[:-1] + ","
				#this is taking what the previous calc function did(returning list of shit)
				#taking out the beggining bracket [
				#data_values = data_values[1:] 
			#place whatever in the text field
			data_values = str(old_string) + str(instance_value_calc)
			self.calculation_switch = False
			#print "first data transmit"
		else:
			old_string = self.bb_temp_text_edit.toPlainText()
			#print "old_string[:-1]"
			#print old_string[:-1]
			old_string = old_string + ", "
			data_values = str(old_string) + str(instance_value_calc[1:])
			#print "data transmite"

		self.dewar_temp_text_edit.clear()
		print(data_values)
		self.dewar_temp_text_edit.insertPlainText(data_values)
		#print "made it past redraw"

#is what the thread signal emits to 
#this function will take the data store it and populate it into the corresponding text 
#fields
	def add_value_text_ivt(self,instance_value_calc):

		#print "instance_value_calc"
		#print instance_value_calc

		if self.calculation_switch_ivt == True:
			#check the text box for what the previous data was
			old_string = self.bb_temp_text_edit.toPlainText()
			#take out the brackets and add a comma
			#print old_string
			check = "[]"
			if old_string == check or old_string == "":
				self.bb_temp_text_edit.clear()
				old_string = ""
			else:
				old_string = old_string[:-1] + ","
				#this is taking what the previous calc function did(returning list of shit)
				#taking out the beggining bracket [
				#data_values = data_values[1:] 
			#place whatever in the text field
			data_values = str(old_string) + str(instance_value_calc)
			self.calculation_switch = False
			#print "first data transmit"
		else:
			old_string = self.bb_temp_text_edit.toPlainText()
			#print "old_string[:-1]"
			#print old_string[:-1]
			old_string = old_string + ", "
			data_values = str(old_string) + str(instance_value_calc[1:])
			#print "data transmite"

		self.ivt_text_edit.clear()
		self.ivt_text_edit.insertPlainText(data_values)
		#print "made it past redraw"
		
#when the thread is done 
#a message will be displayed saying so
	def done(self):
		QtGui.QMessageBox.information(self,"Done!","Done calculating data")

#runs the gui
#and starts shit
def main():
	app = QtGui.QApplication(sys.argv)
	form = PyNyxe()
	form.show()
	app.exec_()


if __name__ =='__main__':
	main()



